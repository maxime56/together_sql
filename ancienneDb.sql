-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema boegbjzol9xmzfhwchc7
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `boegbjzol9xmzfhwchc7` ;

-- -----------------------------------------------------
-- Schema boegbjzol9xmzfhwchc7
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `boegbjzol9xmzfhwchc7` DEFAULT CHARACTER SET latin1 ;
USE `boegbjzol9xmzfhwchc7` ;

-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Country` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Country` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Address` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Address` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `postalCode` VARCHAR(50) NOT NULL,
  `streetName` VARCHAR(60) NOT NULL,
  `city` VARCHAR(60) NOT NULL,
  `Country_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `Country_id`),
  INDEX `fk_Address_Country1_idx` (`Country_id` ASC),
  CONSTRAINT `fk_Address_Country1`
    FOREIGN KEY (`Country_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Center`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(70) NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  `phoneNumber` VARCHAR(60) NOT NULL,
  `isActive` TINYINT(4) NOT NULL,
  `schedule` VARCHAR(150) NOT NULL,
  `Address_id` INT(11) NOT NULL,
  `Country_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `Country_id`),
  INDEX `fk_Center_Address1_idx` (`Address_id` ASC),
  INDEX `fk_Center_Country1_idx` (`Country_id` ASC),
  CONSTRAINT `fk_Center_Address1`
    FOREIGN KEY (`Address_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Center_Country1`
    FOREIGN KEY (`Country_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Race`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Race` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Race` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Species`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Species` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Species` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Animal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Animal` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Animal` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `description` VARCHAR(150) NOT NULL,
  `age` VARCHAR(45) NOT NULL,
  `gender` VARCHAR(30) NOT NULL,
  `birthDate` DATE NOT NULL,
  `isActive` TINYINT(4) NOT NULL,
  `documentRef` VARCHAR(75) NOT NULL,
  `Center_id` INT(11) NOT NULL,
  `Race_id` INT(11) NOT NULL,
  `Species_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `Center_id`, `Race_id`, `Species_id`),
  INDEX `fk_Animal_Center1_idx` (`Center_id` ASC),
  INDEX `fk_Animal_Race1_idx` (`Race_id` ASC),
  INDEX `fk_Animal_Species1_idx` (`Species_id` ASC),
  CONSTRAINT `fk_Animal_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Animal_Race1`
    FOREIGN KEY (`Race_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Race` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Animal_Species1`
    FOREIGN KEY (`Species_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Species` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`User` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`User` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(70) NOT NULL UNIQUE,
  `firstName` VARCHAR(60) NOT NULL,
  `lastName` VARCHAR(60) NOT NULL,
  `phoneNumber` VARCHAR(60) NOT NULL,
  `password` VARCHAR(350) NOT NULL,
  `lastConnexion` DATE NOT NULL,
  `Address_id` INT(11),
  PRIMARY KEY (`id`),
  INDEX `fk_User_Address_idx` (`Address_id` ASC),
  CONSTRAINT `fk_User_Address`
    FOREIGN KEY (`Address_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Adoption`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Adoption` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Adoption` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `User_id` INT(11) NOT NULL,
  `Animal_id` INT(11) NOT NULL,
  `Animal_Center_id` INT(11) NOT NULL,
  `isActive` TINYINT(4) NOT NULL,
  `adoptionDate` DATE NOT NULL,
  PRIMARY KEY (`id`, `User_id`, `Animal_id`, `Animal_Center_id`),
  INDEX `fk_User_has_Animal_Animal1_idx` (`Animal_id` ASC, `Animal_Center_id` ASC),
  INDEX `fk_User_has_Animal_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_User_has_Animal_Animal1`
    FOREIGN KEY (`Animal_id` , `Animal_Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Animal` (`id` , `Center_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Animal_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Offer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Offer` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Offer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL,
  `description` VARCHAR(400) NOT NULL,
  `startDate` DATE NOT NULL,
  `endDate` DATE NOT NULL,
  `place` INT(11) NOT NULL,
  `Center_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `Center_id`),
  INDEX `fk_Offer_Center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_Offer_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Candidat`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Candidat` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Candidat` (
  `User_id` INT(11) NOT NULL,
  `Offer_id` INT(11) NOT NULL,
  `Offer_Center_id` INT(11) NOT NULL,
  `landedDate` DATE NOT NULL,
  `isAccepted` TINYINT(4) NOT NULL,
  `isActive` TINYINT(4) NOT NULL,
  PRIMARY KEY (`User_id`, `Offer_id`, `Offer_Center_id`),
  INDEX `fk_User_has_Offer_Offer1_idx` (`Offer_id` ASC, `Offer_Center_id` ASC),
  INDEX `fk_User_has_Offer_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_User_has_Offer_Offer1`
    FOREIGN KEY (`Offer_id` , `Offer_Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Offer` (`id` , `Center_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Center_race`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center_race` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center_race` (
  `Center_id` INT(11) NOT NULL,
  `Race_id` INT(11) NOT NULL,
  PRIMARY KEY (`Center_id`, `Race_id`),
  INDEX `fk_Center_has_Race_Race1_idx` (`Race_id` ASC),
  INDEX `fk_Center_has_Race_Center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_Center_has_Race_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Center_has_Race_Race1`
    FOREIGN KEY (`Race_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Race` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Center_species`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center_species` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center_species` (
  `Center_id` INT(11) NOT NULL,
  `Species_id` INT(11) NOT NULL,
  PRIMARY KEY (`Center_id`, `Species_id`),
  INDEX `fk_Center_has_Species_Species1_idx` (`Species_id` ASC),
  INDEX `fk_Center_has_Species_Center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_Center_has_Species_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Center_has_Species_Species1`
    FOREIGN KEY (`Species_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Species` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Picture`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Picture` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Picture` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(250) NOT NULL,
  `Animal_id` INT(11) NULL DEFAULT NULL,
  `User_id` INT(11) NULL DEFAULT NULL,
  `Offer_id` INT(11) NULL DEFAULT NULL,
  `Center_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_picture_animal1_idx` (`Animal_id` ASC),
  INDEX `fk_picture_user1_idx` (`User_id` ASC),
  INDEX `fk_picture_offer1_idx` (`Offer_id` ASC),
  INDEX `fk_picture_center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_picture_animal1`
    FOREIGN KEY (`Animal_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Animal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_picture_center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_picture_offer1`
    FOREIGN KEY (`Offer_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Offer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_picture_user1`
    FOREIGN KEY (`User_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Role` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`User_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`User_role` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`User_role` (
  `Role_id` INT(11) NOT NULL,
  `User_id` INT(11) NOT NULL,
  `active` DATE NOT NULL,
  `expiration` DATE NOT NULL,
  PRIMARY KEY (`Role_id`, `User_id`),
  INDEX `fk_User_role_has_user_user1_idx` (`User_id` ASC),
  INDEX `fk_User_role_has_user_User_role1_idx` (`Role_id` ASC),
  CONSTRAINT `fk_User_role_has_user_User_role1`
    FOREIGN KEY (`Role_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_role_has_user_user1`
    FOREIGN KEY (`User_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Vaccine`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Vaccine_animal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_animal` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_animal` (
  `Vaccine_id` INT(11) NOT NULL,
  `Animal_id` INT(11) NOT NULL,
  `vaccinationDate` DATE NOT NULL,
  `nextVaccination` DATE NOT NULL,
  PRIMARY KEY (`Vaccine_id`, `Animal_id`),
  INDEX `fk_Vaccine_has_Animal_Animal1_idx` (`Animal_id` ASC),
  INDEX `fk_Vaccine_has_Animal_Vaccine1_idx` (`Vaccine_id` ASC),
  CONSTRAINT `fk_Vaccine_has_Animal_Animal1`
    FOREIGN KEY (`Animal_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Animal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Vaccine_has_Animal_Vaccine1`
    FOREIGN KEY (`Vaccine_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Vaccine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Vaccine_center`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_center` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_center` (
  `Vaccine_id` INT(11) NOT NULL,
  `Center_id` INT(11) NOT NULL,
  PRIMARY KEY (`Vaccine_id`, `Center_id`),
  INDEX `fk_Vaccine_has_Center_Center1_idx` (`Center_id` ASC),
  INDEX `fk_Vaccine_has_Center_Vaccine1_idx` (`Vaccine_id` ASC),
  CONSTRAINT `fk_Vaccine_has_Center_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Vaccine_has_Center_Vaccine1`
    FOREIGN KEY (`Vaccine_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Vaccine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Center_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center_role` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center_role` (
  `Role_id` INT(11) NOT NULL,
  `Center_id` INT(11) NOT NULL,
  PRIMARY KEY (`Role_id`, `Center_id`),
  INDEX `fk_role_has_center_center1_idx` (`Center_id` ASC),
  INDEX `fk_role_has_center_role1_idx` (`Role_id` ASC),
  CONSTRAINT `fk_role_has_center_role1`
    FOREIGN KEY (`Role_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_has_center_center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
