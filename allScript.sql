-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';
SET SQL_SAFE_UPDATES = 0;
set global max_connections = 550;
-- -----------------------------------------------------

-- Schema boegbjzol9xmzfhwchc7
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `boegbjzol9xmzfhwchc7` ;

-- -----------------------------------------------------
-- Schema boegbjzol9xmzfhwchc7
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `boegbjzol9xmzfhwchc7` DEFAULT CHARACTER SET latin1 ;
USE `boegbjzol9xmzfhwchc7`;

-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Country` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Country` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name` (`name` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Address` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Address` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `postalCode` VARCHAR(50) NOT NULL,
  `streetName` VARCHAR(60) NOT NULL,
  `city` VARCHAR(60) NOT NULL,
  `Country_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `Country_id`),
  INDEX `fk_Address_Country1_idx` (`Country_id` ASC),
  CONSTRAINT `fk_Address_Country1`
    FOREIGN KEY (`Country_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`center`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(70) NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  `phoneNumber` VARCHAR(60) NOT NULL,
  `isActive` TINYINT(4) NOT NULL,
  `schedule` VARCHAR(150) NOT NULL,
  `Address_id` INT(11),
  `Country_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `Country_id`),
  INDEX `fk_Center_Address1_idx` (`Address_id` ASC),
  INDEX `fk_Center_Country1_idx` (`Country_id` ASC),
  CONSTRAINT `fk_Center_Address1`
    FOREIGN KEY (`Address_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Center_Country1`
    FOREIGN KEY (`Country_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;
ALTER TABLE `boegbjzol9xmzfhwchc7`.`Center` 
ADD UNIQUE INDEX `Address_id_UNIQUE` (`Address_id` ASC) ;
;



-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`race`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Race` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Race` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`species`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Species` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Species` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`animal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Animal` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Animal` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `description` VARCHAR(150) NOT NULL,
  `age` VARCHAR(45) NOT NULL,
  `gender` VARCHAR(30) NOT NULL,
  `birthDate` DATE NOT NULL,
  `isActive` TINYINT(4) NOT NULL,
  `documentRef` VARCHAR(75) NOT NULL,
  `Center_id` INT(11) NOT NULL,
  `Race_id` INT(11) NOT NULL,
  `Species_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `Center_id`, `Race_id`, `Species_id`),
  INDEX `fk_Animal_Center1_idx` (`Center_id` ASC),
  INDEX `fk_Animal_Race1_idx` (`Race_id` ASC),
  INDEX `fk_Animal_Species1_idx` (`Species_id` ASC),
  CONSTRAINT `fk_Animal_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Animal_Race1`
    FOREIGN KEY (`Race_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Race` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Animal_Species1`
    FOREIGN KEY (`Species_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Species` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`User` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`User` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(70) NOT NULL,	
  `firstName` VARCHAR(60) NOT NULL,
  `lastName` VARCHAR(60) NOT NULL,
  `phoneNumber` VARCHAR(60) NOT NULL,
  `password` VARCHAR(350) NOT NULL,
  `lastConnexion` DATE NOT NULL,
  `Address_id` INT(11) UNIQUE,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email` (`email` ASC),
  INDEX `fk_User_Address_idx` (`Address_id` ASC),
  CONSTRAINT `fk_User_Address`
    FOREIGN KEY (`Address_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;
ALTER TABLE `boegbjzol9xmzfhwchc7`.`User` 
ADD UNIQUE INDEX `Address_id_UNIQUE` (`Address_id` ASC);
;



-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`adoption`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Adoption` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Adoption` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `User_id` INT(11) NOT NULL,
  `Animal_id` INT(11) NOT NULL,
  `Animal_Center_id` INT(11) NOT NULL,
  `isActive` TINYINT(4) NOT NULL,
  `adoptionDate` DATE NOT NULL,
  PRIMARY KEY (`id`, `User_id`, `Animal_id`, `Animal_Center_id`),
  INDEX `fk_User_has_Animal_Animal1_idx` (`Animal_id` ASC, `Animal_Center_id` ASC),
  INDEX `fk_User_has_Animal_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_User_has_Animal_Animal1`
    FOREIGN KEY (`Animal_id` , `Animal_Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Animal` (`id` , `Center_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Animal_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`offer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Offer` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Offer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL,
  `description` VARCHAR(400) NOT NULL,
  `startDate` DATE NOT NULL,
  `endDate` DATE NOT NULL,
  `place` INT(11) NOT NULL,
  `Center_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `Center_id`),
  INDEX `fk_Offer_Center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_Offer_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`candidat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Candidat` (
  `User_id` INT(11) NOT NULL,
  `Offer_id` INT(11) NOT NULL,
  `Offer_Center_id` INT(11) NOT NULL,
  `landedDate` DATE NOT NULL,
  `isAccepted` TINYINT(4) NOT NULL,
  `isActive` TINYINT(4) NOT NULL,
  PRIMARY KEY (`User_id`, `Offer_id`, `Offer_Center_id`),
  INDEX `fk_User_has_Offer_Offer1_idx` (`Offer_Center_id` ASC),
  INDEX `fk_User_has_Offer_User1_idx` (`User_id` ASC),
  FOREIGN KEY (Offer_Center_id) REFERENCES Center(id)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`center_race`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center_race` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center_race` (
  `Center_id` INT(11) NOT NULL,
  `Race_id` INT(11) NOT NULL,
  PRIMARY KEY (`Center_id`, `Race_id`),
  INDEX `fk_Center_has_Race_Race1_idx` (`Race_id` ASC),
  INDEX `fk_Center_has_Race_Center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_Center_has_Race_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Center_has_Race_Race1`
    FOREIGN KEY (`Race_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Race` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Role` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`center_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center_role` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center_role` (
  `Role_id` INT(11) NOT NULL,
  `Center_id` INT(11) NOT NULL,
  PRIMARY KEY (`Role_id`, `Center_id`),
  INDEX `fk_role_has_center_center1_idx` (`Center_id` ASC),
  INDEX `fk_role_has_center_role1_idx` (`Role_id` ASC),
  CONSTRAINT `fk_role_has_center_center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_has_center_role1`
    FOREIGN KEY (`Role_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`center_species`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center_species` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center_species` (
  `Center_id` INT(11) NOT NULL,
  `Species_id` INT(11) NOT NULL,
  PRIMARY KEY (`Center_id`, `Species_id`),
  INDEX `fk_Center_has_Species_Species1_idx` (`Species_id` ASC),
  INDEX `fk_Center_has_Species_Center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_Center_has_Species_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Center_has_Species_Species1`
    FOREIGN KEY (`Species_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Species` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`picture`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Picture` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Picture` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(250) NOT NULL,
  `Animal_id` INT(11) NULL DEFAULT NULL,
  `User_id` INT(11) NULL DEFAULT NULL,
  `Offer_id` INT(11) NULL DEFAULT NULL,
  `Center_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_picture_animal1_idx` (`Animal_id` ASC),
  INDEX `fk_picture_user1_idx` (`User_id` ASC),
  INDEX `fk_picture_offer1_idx` (`Offer_id` ASC),
  INDEX `fk_picture_center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_picture_animal1`
    FOREIGN KEY (`Animal_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Animal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_picture_center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_picture_offer1`
    FOREIGN KEY (`Offer_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Offer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_picture_user1`
    FOREIGN KEY (`User_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`User_role` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`User_role` (
  `Role_id` INT(11) NOT NULL,
  `User_id` INT(11) NOT NULL,
  `active` DATE NOT NULL,
  `expiration` DATE NOT NULL,
  `Center_id` INT NOT NULL,
  PRIMARY KEY (`Role_id`, `User_id`),
  INDEX `fk_User_role_has_user_user1_idx` (`User_id` ASC),
  INDEX `fk_User_role_has_user_User_role1_idx` (`Role_id` ASC),
  INDEX `fk_User_role_has_center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_User_role_has_user_User_role1`
    FOREIGN KEY (`Role_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_role_has_user_user1`
    FOREIGN KEY (`User_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_role_has_center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`vaccine`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`vaccine_animal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_animal` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_animal` (
  `Vaccine_id` INT(11) NOT NULL,
  `Animal_id` INT(11) NOT NULL,
  `vaccinationDate` DATE NOT NULL,
  `nextVaccination` DATE NOT NULL,
  PRIMARY KEY (`Vaccine_id`, `Animal_id`),
  INDEX `fk_Vaccine_has_Animal_Animal1_idx` (`Animal_id` ASC),
  INDEX `fk_Vaccine_has_Animal_Vaccine1_idx` (`Vaccine_id` ASC),
  CONSTRAINT `fk_Vaccine_has_Animal_Animal1`
    FOREIGN KEY (`Animal_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Animal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Vaccine_has_Animal_Vaccine1`
    FOREIGN KEY (`Vaccine_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Vaccine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`vaccine_center`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_center` ;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_center` (
  `Vaccine_id` INT(11) NOT NULL,
  `Center_id` INT(11) NOT NULL,
  PRIMARY KEY (`Vaccine_id`, `Center_id`),
  INDEX `fk_Vaccine_has_Center_Center1_idx` (`Center_id` ASC),
  INDEX `fk_Vaccine_has_Center_Vaccine1_idx` (`Vaccine_id` ASC),
  CONSTRAINT `fk_Vaccine_has_Center_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Vaccine_has_Center_Vaccine1`
    FOREIGN KEY (`Vaccine_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Vaccine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

-- supprime un utilisateur

DROP PROCEDURE IF EXISTS delete_user;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_user`(
	IN n_id INT
)
BEGIN  
	DELETE FROM User WHERE id = n_id;
END$$
DELIMITER ;


-- supprime un animal

DROP PROCEDURE IF EXISTS delete_animal;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_animal`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Animal WHERE id = n_id;
END$$
DELIMITER ;


-- supprime une offre d'un centre

DROP PROCEDURE IF EXISTS delete_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_offer`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Offer WHERE id = n_id;
END$$
DELIMITER ;


-- supprime une adresse

DROP PROCEDURE IF EXISTS delete_address;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_address`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Address WHERE id = n_id;
END$$
DELIMITER ;


-- supprime un candidat

DROP PROCEDURE IF EXISTS delete_candidat;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_candidat`(
	IN n_id INT,
	IN n_offerId INT
)
BEGIN  
	DELETE FROM Candidat WHERE User_id = n_id AND Offer_id = n_offerId;
END$$
DELIMITER ;


-- supprime les candidatures d'un utilisateur

DROP PROCEDURE IF EXISTS delete_all_user_application;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_all_user_application`(
	IN n_userId INT
)
BEGIN  	
	DELETE FROM Candidat WHERE User_id = n_userId;
END$$
DELIMITER ;


-- supprime une photo

DROP PROCEDURE IF EXISTS delete_picutre;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_picutre`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Picture WHERE id = n_id;
END$$
DELIMITER ;

-- supprime les photos d'un utisateur (il doit avoir normalement une seul photo)

DROP PROCEDURE IF EXISTS delete__user_picutre;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete__user_picutre`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Picture WHERE User_id = n_id;
END$$
DELIMITER ;


-- supprime un rôle ainsi que sa connexion entre les utilisateurs et les centres

DROP PROCEDURE IF EXISTS delete_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_role`(
	IN n_role_id INT
)
BEGIN  
	DELETE FROM Role WHERE id = n_role_id;
END$$
DELIMITER ;


-- supprime un vaccin ainsi que sa connexion entre les centres, cependant, si un animal est associé au vaccin, la suppression ne va pas marcher

DROP PROCEDURE IF EXISTS delete_vaccine;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_vaccine`(
	IN n_vaccine_id INT
)
BEGIN  
	DELETE FROM Vaccine WHERE id = n_vaccine_id;
END$$
DELIMITER ;


-- supprime une race ainsi que sa connexion entre les centres, cependant, si un animal est associé à la race, la suppression ne va pas marcher

DROP PROCEDURE IF EXISTS delete_race;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_race`(
	IN n_race_id INT
)
BEGIN  
 	DELETE FROM Race WHERE id = n_race_id;
END$$
DELIMITER ;


-- supprime une espèce ainsi que sa connexion entre les centres, cependant, si un animal est associé à une espèce, la suppression ne va pas marcher

DROP PROCEDURE IF EXISTS delete_species;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_species`(
	IN n_species_id INT
)
BEGIN  
 	DELETE FROM Species WHERE id = n_species_id;
END$$
DELIMITER ;


-- supprime un animal vacciné

DROP PROCEDURE IF EXISTS delete_animal_vaccined;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_animal_vaccined`(
	IN n_Vaccine_id INT,
	IN n_Animal_id INT
)
BEGIN  
 	DELETE FROM Vaccine_animal WHERE Vaccine_id = n_Vaccine_id AND Animal_id = n_Animal_id;
END$$
DELIMITER ;


-- supprime un userRole

DROP PROCEDURE IF EXISTS delete_userRole;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_userRole`(
	IN n_Role_id INT,
	IN n_User_id INT
)
BEGIN 
 	DELETE FROM User_role WHERE Role_id = n_Role_id AND User_id = n_User_id;
END$$
DELIMITER ;use boegbjzol9xmzfhwchc7;

-- supprime toutes les offres d'un centre
DROP PROCEDURE IF EXISTS delete_center_offers;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_offers`(
	IN n_id INT
)
BEGIN  
	DELETE Picture FROM boegbjzol9xmzfhwchc7.Picture INNER JOIN Offer WHERE Offer.id = Picture.Offer_id AND Offer.Center_id = n_id;
	DELETE FROM Offer WHERE Center_id = n_id;
END$$
DELIMITER ;

-- supprime les photos d'un animal

DROP PROCEDURE IF EXISTS delete_animal_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_animal_picture`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Picture WHERE Animal_id = n_id;
END$$
DELIMITER ;


-- supprime les vaccins d'un animal

DROP PROCEDURE IF EXISTS delete_animal_vaccines;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_animal_vaccines`(
	IN n_AnimalId INT
)
BEGIN  
	DELETE FROM Vaccine_animal WHERE Animal_id = n_AnimalId;
END$$
DELIMITER ;


-- supprime le ou les adoptions d'un animal
DROP PROCEDURE IF EXISTS delete_animal_adoption;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_animal_adoption`(
	IN n_AnimalId INT
)
BEGIN  
	DELETE FROM Adoption WHERE Animal_id = n_AnimalId;
END$$
DELIMITER ;
-- supprime les photos d'un centre 

DROP PROCEDURE IF EXISTS delete_center_pictures;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_pictures`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Picture WHERE Center_id = n_id;
END$$
DELIMITER ;

-- supprime les vaccins qui sont liés à un centre
DROP PROCEDURE IF EXISTS delete_center_vaccines;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_vaccines`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Vaccine_center WHERE Center_id = n_id;
END$$
DELIMITER ;

-- supprime les roles qui sont liés à un centre
DROP PROCEDURE IF EXISTS delete_center_userRole;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_userRole`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Center_role WHERE Center_id = n_id;
    DELETE FROM User_role WHERE Center_id = n_id;
END$$
DELIMITER ;

-- supprime les especes qui sont liés à un centre
DROP PROCEDURE IF EXISTS delete_center_animals;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_animals`(
	IN n_id INT
)
BEGIN  
	DELETE Vaccine_animal FROM boegbjzol9xmzfhwchc7.Vaccine_animal INNER JOIN Animal WHERE Animal.id = Vaccine_animal.Animal_id AND Animal.Center_id = n_id;
	DELETE Picture FROM boegbjzol9xmzfhwchc7.Picture INNER JOIN Animal WHERE Animal.id = Picture.Animal_id AND Animal.Center_id = n_id;
	DELETE FROM Animal Where Center_id = n_id;
END$$
DELIMITER ;

-- supprime les especes qui sont liés à un centre
DROP PROCEDURE IF EXISTS delete_center_species;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_species`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Center_species WHERE Center_id = n_id;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS delete_center_adoptions;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_adoptions`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Adoption WHERE Animal_Center_id = n_id;
END$$
DELIMITER ;

-- supprime un centre
DROP PROCEDURE IF EXISTS delete_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Center WHERE id = n_id;
END$$
DELIMITER ;

-- supprime les candidats qui ont postulés aux offres d'un centre

DROP PROCEDURE IF EXISTS delete_center_candidat;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_candidat`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Candidat WHERE Offer_Center_id = n_id;
END$$
DELIMITER ;

-- //////////////////////////////
-- ******************************
-- PROCEDURE DE SELECTION MULTIPLE
-- ******************************
-- //////////////////////////////


-- selectionne les races disponible

DROP PROCEDURE IF EXISTS select_all_races;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_races`(

)
BEGIN
    SELECT * FROM Race;
END$$
DELIMITER ;

-- selectionne les Vaccins disponible

DROP PROCEDURE IF EXISTS select_all_vaccines;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_vaccines`(

)
BEGIN
    SELECT * FROM Vaccine;
END$$
DELIMITER ;

-- selectionne les Roles disponible

DROP PROCEDURE IF EXISTS select_all_roles;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_roles`(

)
BEGIN
    SELECT * FROM Role;
END$$
DELIMITER ;

-- selectionne les espèces disponible

DROP PROCEDURE IF EXISTS select_all_species;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_species`(

)
BEGIN
    SELECT * FROM Species;
END$$
DELIMITER ;


-- selectionne les centres

DROP PROCEDURE IF EXISTS select_centers;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_centers`(
    IN n_limit INT,
	IN n_offset INT
)
BEGIN
    SELECT * FROM Center LIMIT n_limit OFFSET n_offset;
END$$
DELIMITER ;


-- selectionne les centres avec les adresses

DROP PROCEDURE IF EXISTS select_centers_with_address_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_centers_with_address_picture`(
	IN n_addressId INT
)
BEGIN
    SELECT * FROM Center INNER JOIN Address WHERE Center.Address_id = Address.id;
END$$
DELIMITER ;


-- selectionne les animaux par centre

DROP PROCEDURE IF EXISTS select_animals;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animals`(
	IN id_center INT,
    IN n_limit INT,
	IN n_offset INT
)
BEGIN
	SELECT * FROM Animal WHERE Center_id = id_center LIMIT n_limit OFFSET n_offset;
END$$
DELIMITER ;


-- selectionne les pays disponible

DROP PROCEDURE IF EXISTS select_countrys;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_countrys`(
    IN n_limit INT,
	IN n_offset INT
)
BEGIN
    SELECT * FROM Country LIMIT n_limit OFFSET n_offset;
END$$
DELIMITER ;

-- selectionne les roles disponible

DROP PROCEDURE IF EXISTS select_roles_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_roles_by_center`(
	IN n_center_id INT
)
BEGIN
    SELECT * FROM Center_role INNER JOIN Role WHERE Role.id = Center_role.Role_id AND  Center_id = n_center_id;
END$$
DELIMITER ;



-- selectionne les roles disponible qui sont lié à un centre

DROP PROCEDURE IF EXISTS select_center_roles;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_center_roles`(
	IN n_center_id INT
)
BEGIN
    SELECT * FROM Role INNER JOIN Center_role ON Center_role.Center_id = n_center_id;
END$$
DELIMITER ;


-- selectionne la table de jointure les utilisateurs qui ont des rôles dans un centre

DROP PROCEDURE IF EXISTS select_user_with_role_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user_with_role_by_center`(
	IN n_center_id INT
)
BEGIN
	SELECT DISTINCT * FROM User_role WHere Center_id = n_center_id;
END$$
DELIMITER ;


-- selectionne les vaccins disponible qui sont lié à un centre

DROP PROCEDURE IF EXISTS select_center_vaccine;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_center_vaccine`(
	IN centerId INT
)
BEGIN
	SELECT DISTINCT * FROM Vaccine INNER JOIN Vaccine_center WHERE Vaccine_center.Center_id = centerId AND Vaccine.id = Vaccine_center.Vaccine_id;
END$$
DELIMITER ;

-- selectionne les races disponible qui sont lié à un centre

DROP PROCEDURE IF EXISTS select_all_races_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_races_by_center`(
	IN CenterId INT
)
BEGIN
    SELECT DISTINCT * FROM Race INNER JOIN Center_race WHERE Center_race.Center_id = CenterId AND Center_race.Race_id = Race.id;
END$$
DELIMITER ;


-- selectionne les espèces disponible qui sont lié à un centre

DROP PROCEDURE IF EXISTS select_all_species_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_species_by_center`(
	IN n_center_id INT
)
BEGIN
    SELECT DISTINCT * FROM Species inner join Center_species WHERE Center_species.Center_id = n_center_id AND Center_species.Species_id = Species.id;
END$$
DELIMITER ;


-- selectionne les offres dans un centre

DROP PROCEDURE IF EXISTS select_center_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_center_offer`(
	IN n_center_id INT
)
BEGIN
    SELECT * FROM Offer WHERE Center_id = n_center_id;
END$$
DELIMITER ;

-- selectionne les candidats ont postulés à une offre

DROP PROCEDURE IF EXISTS select_candidats_by_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_candidats_by_offer`(
	IN n_offer_id INT
)
BEGIN
    SELECT * FROM Candidat INNER JOIN User WHERE Candidat.User_id = User.id AND Offer_id = n_offer_id;
END$$
DELIMITER ;

-- selectionne les offres d'un candidat

DROP PROCEDURE IF EXISTS select_candidat_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_candidat_offer`(
	IN n_User_id INT
)
BEGIN  
	SELECT * FROM Offer INNER JOIN Candidat ON Candidat.User_id = n_User_id AND Candidat.Offer_id = id;
END$$
DELIMITER ;

-- selectionne les adoptions d'un centre

DROP PROCEDURE IF EXISTS select_adoption_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_adoption_by_center`(
	IN n_center_id INT
)
BEGIN  
	SELECT * FROM Adoption WHERE Animal_Center_id = n_center_id;
END$$
DELIMITER ;


-- selectionne les adoptions d'un utilisateur

DROP PROCEDURE IF EXISTS select_adoption_by_user;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_adoption_by_user`(
	IN n_user_id INT
)
BEGIN  
	SELECT * FROM Adoption WHERE User_id = n_user_id;
END$$
DELIMITER ;


-- selectionne les animaux vaccinés par un même vaccin

DROP PROCEDURE IF EXISTS select_animals_vaccined_by_vaccine;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animals_vaccined_by_vaccine`(
	IN n_Center_id INT,
	IN n_Vaccine_id INT
)
BEGIN  
	SELECT * FROM Vaccine_animal INNER JOIN Animal WHERE Animal.id = Vaccine_animal.Animal_id AND Vaccine_id = n_Vaccine_id AND Animal.Center_id = n_Center_id;
END$$
DELIMITER ;


-- selectionne les animaux vaccinés par centre

DROP PROCEDURE IF EXISTS select_animals_vaccined_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animals_vaccined_by_center`(
	IN n_center_id INT
)
BEGIN  
	SELECT DISTINCT * FROM Vaccine_animal INNER JOIN Animal WHERE Vaccine_animal.Animal_id = Animal.id AND Animal.Center_id = n_center_id;
END$$
DELIMITER ;


-- selectionne les photos d'un centre

DROP PROCEDURE IF EXISTS select_center_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_center_picture`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Picture WHERE Center_id = n_id;
END$$
DELIMITER ;


-- selectionne les photos d'un utilisateur

DROP PROCEDURE IF EXISTS select_user_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user_picture`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Picture WHERE User_id = n_id;
END$$
DELIMITER ;

-- selectionne les photos d'un animal

DROP PROCEDURE IF EXISTS select_animal_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animal_picture`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Picture WHERE Animal_id = n_id;
END$$
DELIMITER ;


-- selectionne les photos d'une offre

DROP PROCEDURE IF EXISTS select_offer_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_offer_picture`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Picture WHERE Offer_id = n_id;
END$$
DELIMITER ;


-- selectionne un utilisateur qui possèdent un rôle dans un centre
DROP PROCEDURE IF EXISTS select_user_with_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user_with_role`(
	IN n_userId INT
)
BEGIN  
	SELECT * FROM User_role WHERE User_id = n_userId;
END$$
DELIMITER ;


-- selectionne les offres auquel un utilisateur à participer

DROP PROCEDURE IF EXISTS select_historic_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_historic_offer`(
	IN n_userId INT
)
BEGIN  
	SELECT * FROM Candidat INNER JOIN Offer WHERE User_id = n_userId;
END$$
DELIMITER ;


-- selectionne les centres par pays avec leur adresse

DROP PROCEDURE IF EXISTS select_country_centers;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_country_centers`(
	IN n_limit INT,
	IN n_offset INT,
	IN n_countryId INT
)
BEGIN  
	SELECT * FROM Center WHERE Country_id = n_countryId LIMIT n_limit OFFSET n_offset;
END$$
DELIMITER ;


-- selectionne les centres

DROP PROCEDURE IF EXISTS select_every_centers;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_every_centers`(
	IN n_limit INT,
	IN n_offset INT
)
BEGIN  
	SELECT * FROM Center LIMIT n_limit OFFSET n_offset;
END$$
DELIMITER ;


-- ///////////////////////////////////////////
-- ******************************************
-- FIN DES PROCEDURES DE SELECTION MULTIPLE
-- ******************************************
-- ///////////////////////////////////////////

use boegbjzol9xmzfhwchc7;
-- //////////////////////////////
-- ******************************
-- PROCEDURE DE SELECTION UNIQUE
-- ******************************
-- //////////////////////////////

-- selectionne un utilisateur

DROP PROCEDURE IF EXISTS select_user;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user`(
	IN n_user_id INT
)
BEGIN  
	SELECT id, email, firstName, lastName, phoneNumber, lastConnexion, Address_id, password FROM User WHERE id = n_user_id;
END$$
DELIMITER ;


-- selectionne une adresse

DROP PROCEDURE IF EXISTS select_address;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_address`(
	IN n_address_id INT
)
BEGIN  
	SELECT * FROM Address WHERE id = n_address_id;
END$$
DELIMITER ;


-- selectionne une adoption

DROP PROCEDURE IF EXISTS select_adoption;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_adoption`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Adoption WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne une espèce

DROP PROCEDURE IF EXISTS select_specie;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_specie`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Species WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne une race

DROP PROCEDURE IF EXISTS select_race;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_race`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Race WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne un vaccin

DROP PROCEDURE IF EXISTS select_vaccin;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_vaccin`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Vaccine WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne une offre

DROP PROCEDURE IF EXISTS select_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_offer`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Offer WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne une offre

DROP PROCEDURE IF EXISTS select_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_center`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Center WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne une offre

DROP PROCEDURE IF EXISTS select_animal;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animal`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Animal WHERE id = n_id;
END$$
DELIMITER ;

-- selectionne un candidat

DROP PROCEDURE IF EXISTS select_candidat;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_candidat`(
	IN n_id INT,
	IN n_offferId INT
)
BEGIN  
	SELECT * FROM Candidat WHERE User_id = n_id AND Offer_id = n_offferId;
END$$
DELIMITER ;


-- selectionne une table de jointure

DROP PROCEDURE IF EXISTS select_animal_vaccined;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animal_vaccined`(
	IN n_VaccineId INT,
	IN n_Animal_id INT
)
BEGIN  
	SELECT * FROM Vaccine_animal WHERE Vaccine_id = n_VaccineId AND Animal_id = n_Animal_id;
END$$
DELIMITER ;


-- selectionne une photo

DROP PROCEDURE IF EXISTS select_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_picture`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Picture WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne un role 

DROP PROCEDURE IF EXISTS select_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_role`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Role WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne un role 

DROP PROCEDURE IF EXISTS select_user_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user_role`(
	IN n_roleId INT,
	IN n_UserId INT
)
BEGIN  
	SELECT * FROM User_role WHERE Role_id = n_roleId AND User_id = n_UserId;
END$$
DELIMITER ;


-- permet de chercher les informations utiles d'un utilisateurs qui ont adoptés un animal

DROP PROCEDURE IF EXISTS select_user_information_adoption;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user_information_adoption`(
	IN n_UserId INT
)
BEGIN  
	SELECT DISTINCT User.id, User.firstName, User.lastName, User.email, User.phoneNumber FROM User INNER JOIN Adoption WHERE Adoption.User_id;
END$$
DELIMITER ;

-- récupère les tous les pays

DROP PROCEDURE IF EXISTS get_countrys;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`get_countrys`(
)
BEGIN  
	SELECT * FROM Country ;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS get_one_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`get_one_center`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Center WHERE Center.id =  n_id;
END$$
DELIMITER ;

-- récupère les tous les pays qui ont un centre dans leur pays

DROP PROCEDURE IF EXISTS get_center_countrys;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`get_center_countrys`(
)
BEGIN  
	SELECT Country.name, Country.id FROM Center INNER JOIN Country WHERE Center.Country_id = Country.id;
END$$
DELIMITER ;

-- ///////////////////////////////////////////
-- ******************************************
-- FIN DES PROCEDURES DE SELECTION UNIQUE
-- ******************************************
-- ///////////////////////////////////////////
use boegbjzol9xmzfhwchc7;


-- modifie un utilisateur

DROP PROCEDURE IF EXISTS update_user;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_user`(
    IN n_email VARCHAR(70),
    IN n_firstName VARCHAR(60),
    IN n_lastName VARCHAR(60),
    IN n_phoneNumber VARCHAR(60),
    IN n_password VARCHAR(350),
    IN n_address_id INT,
	IN n_user_id INT
)
BEGIN  
    UPDATE User
	SET 
    email = n_email,
    firstName = n_firstName,
    lastName = n_lastName,
    phoneNumber = n_phoneNumber,
    password = n_password,
    Address_id = n_address_id
	WHERE id = n_user_id;
END$$
DELIMITER ;



-- modifie une adresse

DROP PROCEDURE IF EXISTS update_address;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_address`(
    IN n_postalCode VARCHAR(50),
    IN n_streetName VARCHAR(60),
    IN n_city VARCHAR(60),
    IN n_Country_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE Address
	SET 
    postalCode = n_postalCode,
    streetName = n_streetName,
    city = n_city,
    Country_id = n_Country_id
	WHERE id = n_id;
END$$
DELIMITER ;


-- modifie la fiche d'un animal

DROP PROCEDURE IF EXISTS update_animal;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_animal`(
    IN n_name VARCHAR(60),
    IN n_description VARCHAR(150),
    IN n_age VARCHAR(45),
    IN n_gender VARCHAR(30),
    IN n_birthDate DATE,
    IN n_isActive BOOLEAN,
    IN n_documentRef VARCHAR(75),
    IN n_Center_id INT,
    IN n_Race_id INT,
    IN n_Species_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE Animal
	SET 
    name = n_name,
    description = n_description,
    age = n_age,
    gender = n_gender,
    birthDate = n_birthDate,
    isActive = n_isActive,
    documentRef = n_documentRef,
    Center_id = n_Center_id,
    Race_id = n_Race_id,
    Species_id = n_Species_id
    WHERE id = n_id;
END$$
DELIMITER ;


-- modifie la fiche d'un centre

DROP PROCEDURE IF EXISTS update_centre;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_centre`(
    IN n_email VARCHAR(70),
    IN n_name VARCHAR(60),
    IN n_phoneNumber VARCHAR(60),
    IN n_isActive BOOLEAN,
    IN n_schedule VARCHAR(150),
    IN n_Address_id INT,
    IN n_Country_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE Center
	SET 
    email = n_email,
    name = n_name,
    phoneNumber = n_phoneNumber,
    isActive = n_isActive,
    schedule = n_schedule,
    Address_id = n_Address_id,
    Country_id = n_Country_id
    WHERE id = n_id;
END$$
DELIMITER ;




-- modifie une photo

DROP PROCEDURE IF EXISTS update_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_picture`(
    IN n_path VARCHAR(250),
    IN n_Animal_id INT,
    IN n_User_id INT,
    IN n_Offer_id INT,
    IN n_Center_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE Picture
	SET 
    path = n_path,
    Animal_id = n_Animal_id,
    User_id = n_User_id,
    Offer_id = n_Offer_id,
    Center_id = n_Center_id
    WHERE id = n_id;
END$$
DELIMITER ;


-- modifie une adoption

DROP PROCEDURE IF EXISTS update_adoption;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_adoption`(
    IN n_User_id INT,
    IN n_Animal_id INT,
    IN n_Animal_Center_id INT,
    IN n_isActive BOOLEAN,
    IN n_adoptionDate DATE,
	IN n_id INT
)
BEGIN   
    UPDATE Adoption
	SET 
    User_id = n_User_id,
    Animal_id = n_Animal_id,
    Animal_Center_id = n_Animal_Center_id,
    isActive = n_isActive,
    adoptionDate = n_adoptionDate
    WHERE id = n_id;
END$$
DELIMITER ;


-- modifie un candidat

DROP PROCEDURE IF EXISTS update_candidat;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_candidat`(
    IN n_User_id INT,
    IN n_Offer_id INT,
    IN n_Offer_Center_id INT,
    IN n_landedDate DATE,
    IN n_isAccepted BOOLEAN,
    IN n_isActive BOOLEAN
)
BEGIN  
    UPDATE Candidat
	SET 
    User_id = n_User_id,
    Offer_id = n_Offer_id,
    Offer_Center_id = n_Offer_Center_id,
    landedDate = n_landedDate,
    isAccepted = n_isAccepted,
    isActive = n_isActive
    WHERE User_id = n_User_id AND Offer_id = n_Offer_id;
END$$
DELIMITER ;


-- modifie un candidat

DROP PROCEDURE IF EXISTS update_candidat;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_candidat`(
    IN n_User_id INT,
    IN n_Offer_id INT,
    IN n_Offer_Center_id INT,
    IN n_landedDate DATE,
    IN n_isAccepted BOOLEAN,
    IN n_isActive BOOLEAN
)
BEGIN  
    UPDATE Candidat
	SET 
    User_id = n_User_id,
    Offer_id = n_Offer_id,
    Offer_Center_id = n_Offer_Center_id,
    landedDate = n_landedDate,
    isAccepted = n_isAccepted,
    isActive = n_isActive
    WHERE User_id = n_User_id AND Offer_id = n_Offer_id;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS update_vaccine;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_vaccine`(
    IN n_name VARCHAR(60),
    IN n_id INT
)
BEGIN  
    UPDATE Vaccine
	SET 
    name = n_name
    WHERE id = n_id;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS update_vaccined_animal;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_vaccined_animal`(
    IN n_Vaccine_id INT,
    IN n_Animal_id INT,
    IN n_vaccinationDate DATE,
    IN n_nextVaccination DATE
)
BEGIN  
    UPDATE Vaccine_animal
	SET 
    vaccinationDate = n_vaccinationDate,
    nextVaccination = n_nextVaccination
    WHERE Vaccine_id = n_Vaccine_id AND Animal_id = n_Animal_id;
END$$
DELIMITER ;

-- permet d'update l'id d'une adresse à un utilisateur

DROP PROCEDURE IF EXISTS link_user_addressId;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`link_user_addressId`(
  IN n_Address_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE User
	SET 
    Address_id = n_Address_id
	WHERE id = n_id;
END$$
DELIMITER ;

-- permet d'update l'id d'une adresse à un center

DROP PROCEDURE IF EXISTS link_center_addressId;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`link_center_addressId`(
  IN n_Address_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE Center
	SET 
    Address_id = n_Address_id
	WHERE id = n_id;
END$$
DELIMITER ;


-- modifie un role d'un centre

DROP PROCEDURE IF EXISTS update_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_role`(
    IN n_name VARCHAR(70),
    IN n_id INT
)
BEGIN  
    UPDATE Role
	SET 
    name = n_name
    WHERE id = n_id;
END$$
DELIMITER ;

-- modifie un role d'un centre

DROP PROCEDURE IF EXISTS update_user_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_user_role`(
   IN n_Role_id INT,
   IN n_User_id INT,
   IN n_active DATE,
   IN n_expiration DATE,
   IN n_Center_id INT
)
BEGIN  
    UPDATE User_role
	SET 
    active = n_active,
    expiration = n_expiration,
    Center_id = n_Center_id
    WHERE Role_id = n_Role_id AND User_id = n_User_id;
END$$
DELIMITER ;


-- modifie une offre

DROP PROCEDURE IF EXISTS update_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_offer`(
    IN n_name VARCHAR(70),
    IN n_description VARCHAR(400),
    IN n_startDate DATE,
    IN n_endDate DATE,
    IN n_place INT,
    IN n_Center_id INT,
    IN n_id INT
)
BEGIN  
    UPDATE Offer
	SET 
    name = n_name,
    description = n_description,
    startDate = n_startDate,
    endDate = n_endDate,
    place = n_place,
    Center_id = n_Center_id
    WHERE id = n_id;
END$$
DELIMITER ;


-- modifie une race d'un centre

DROP PROCEDURE IF EXISTS update_race;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_race`(
    IN n_name VARCHAR(60),
    IN n_id INT
)
BEGIN  
    UPDATE Race
	SET 
    name = n_name
    WHERE id = n_id;
END$$
DELIMITER ;

-- modifie une espèce d'un centre

DROP PROCEDURE IF EXISTS update_species;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_species`(
    IN n_name VARCHAR(60),
    IN n_id INT
)
BEGIN  
    UPDATE Species
	SET 
    name = n_name
    WHERE id = n_id;
END$$
DELIMITER ;



USE boegbjzol9xmzfhwchc7;
INSERT INTO Country
 
(name)
 
VALUES
 
('France'),
('Afghanistan'),
('Albania'),
('Algeria'),
('Andorra'),
('Angola'),
('Antigua and Barbuda'),
('Argentina'),
('Armenia'),
('Australia'),
('Austria'),
('Azerbaijan'),
('Bahamas, The'),
('Bahrain'),
('Bangladesh'),
('Barbados'),
('Belarus'),
('Belgium'),
('Belize'),
('Benin'),
('Bhutan'),
('Bolivia'),
('Bosnia and Herzegovina'),
('Botswana'),
('Brazil'),
('Brunei'),
('Bulgaria'),
('Burkina Faso'),
('Burma'),
('Burundi'),
('Cambodia'),
('Cameroon'),
('Canada'),
('Cape Verde'),
('Central Africa'),
('Chad'),
('Chile'),
('China'),
('Colombia'),
('Comoros'),
('Congo, Democratic Republic of the'),
('Costa Rica'),
('Cote dIvoire'),
('Crete'),
('Croatia'),
('Cuba'),
('Cyprus'),
('Czech Republic'),
('Denmark'),
('Djibouti'),
('Dominican Republic'),
('East Timor'),
('Ecuador'),
('Egypt'),
('El Salvador'),
('Equatorial Guinea'),
('Eritrea'),
('Estonia'),
('Ethiopia'),
('Fiji'),
('Finland'),
('Gabon'),
('Gambia, The'),
('Georgia'),
('Germany'),
('Ghana'),
('Greece'),
('Grenada'),
('Guadeloupe'),
('Guatemala'),
('Guinea'),
('Guinea-Bissau'),
('Guyana'),
('Haiti'),
('Holy See'),
('Honduras'),
('Hong Kong'),
('Hungary'),
('Iceland'),
('India'),
('Indonesia'),
('Iran'),
('Iraq'),
('Ireland'),
('Israel'),
('Italy'),
('Ivory Coast'),
('Jamaica'),
('Japan'),
('Jordan'),
('Kazakhstan'),
('Kenya'),
('Kiribati'),
('Korea, North'),
('Korea, South'),
('Kosovo'),
('Kuwait'),
('Kyrgyzstan'),
('Laos'),
('Latvia'),
('Lebanon'),
('Lesotho'),
('Liberia'),
('Libya'),
('Liechtenstein'),
('Lithuania'),
('Macau'),
('Macedonia'),
('Madagascar'),
('Malawi'),
('Malaysia'),
('Maldives'),
('Mali'),
('Malta'),
('Marshall Islands'),
('Mauritania'),
('Mauritius'),
('Mexico'),
('Micronesia'),
('Moldova'),
('Monaco'),
('Mongolia'),
('Montenegro'),
('Morocco'),
('Mozambique'),
('Namibia'),
('Nauru'),
('Nepal'),
('Netherlands'),
('New Zealand'),
('Nicaragua'),
('Niger'),
('Nigeria'),
('North Korea'),
('Norway'),
('Oman'),
('Pakistan'),
('Palau'),
('Panama'),
('Papua New Guinea'),
('Paraguay'),
('Peru'),
('Philippines'),
('Poland'),
('Portugal'),
('Qatar'),
('Romania'),
('Russia'),
('Rwanda'),
('Saint Lucia'),
('Saint Vincent and the Grenadines'),
('Samoa'),
('San Marino'),
('Sao Tome and Principe'),
('Saudi Arabia'),
('Scotland'),
('Senegal'),
('Serbia'),
('Seychelles'),
('Sierra Leone'),
('Singapore'),
('Slovakia'),
('Slovenia'),
('Solomon Islands'),
('Somalia'),
('South Africa'),
('South Korea'),
('Spain'),
('Sri Lanka'),
('Sudan'),
('Suriname'),
('Swaziland'),
('Sweden'),
('Switzerland'),
('Syria'),
('Taiwan'),
('Tajikistan'),
('Tanzania'),
('Thailand'),
('Tibet'),
('Timor-Leste'),
('Togo'),
('Tonga'),
('Trinidad and Tobago'),
('Tunisia'),
('Turkey'),
('Turkmenistan'),
('Tuvalu'),
('Uganda'),
('Ukraine'),
('United Arab Emirates'),
('United Kingdom'),
('United States'),
('Uruguay'),
('Uzbekistan'),
('Vanuatu'),
('Venezuela'),
('Vietnam'),
('Yemen'),
('Zambia'),
('Zimbabwe');
USE boegbjzol9xmzfhwchc7;
USE boegbjzol9xmzfhwchc7;

INSERT INTO Address(postalCode, streetName, city, Country_id)
VALUES ("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1),
("6811", "oui du chêne", 'Lyon', 1);

INSERT INTO User (email, firstName, lastName, phoneNumber, password, lastConnexion, Address_id)
VALUES ("camarchebien@gmail.com", "test", "oui", "8474844", "secret", NOW(), 1),
("azeazeaze@gmail.com", "azeaze", "azeaze", "azeaze", "zzzz", NOW(), 3),
("sapristie@gmail.com", "azeaze", "azeaze", "azeaze", "zzzz", NOW(), 4),
("castex@gmail.com", "azeaze", "azeaze", "azeaze", "zzzz", NOW(), 5),
("macron@gmail.com", "azeaze", "azeaze", "azeaze", "zzzz", NOW(), 6),
("zemour@gmail.com", "azeaze", "azeaze", "azeaze", "zzzz", NOW(), 7),
("melanchon@gmail.com", "azeaze", "azeaze", "azeaze", "zzzz", NOW(), 2),
("macroncon@gmail.com", "azeaze", "azeaze", "azeaze", "zzzz", NOW(), null),
("azzzzzz@gmail.com", "azeaze", "azeaze", "azeaze", "zzzz", NOW(), null);

INSERT INTO Center (email, name, phoneNumber, isActive, schedule, Address_id, Country_id) 
VALUES ("testmail", "Meilleur centre", "929292292", true, "19h50", 8, 1),
("testmail", "Meilleur centre", "929292292", true, "19h50", 9, 1),
("deletePicture", "Meilleur centre", "929292292", true, "19h50", 10, 1),
("deleteCenterDao", "Meilleur centre", "929292292", true, "19h50", 11, 1),
("deleteAllCandidat", "Meilleur centre", "929292292", true, "19h50", 12, 1),
("deleteAllAnimals", "Meilleur centre", "929292292", true, "19h50", 13, 1),
("deleteAlzzlAnimals", "Meilleur centre", "929292292", true, "19h50", null, 1);

INSERT INTO Offer (name, description, startDate, endDate, place, Center_id) VALUES
("testingOffer", "oui", NOW(), NOW(), 10, 1),
("teteeeee", "oui", NOW(), NOW(), 10, 1),
("deleteAllCandidat", "oui", NOW(), NOW(), 10, 1),
("deleteAllOffers", "oui", NOW(), NOW(), 10, 5),
("deleteAllOffers", "oui", NOW(), NOW(), 10, 5);

INSERT INTO Race (name)
VALUES
("Berger allemand"),
("BullDog"),
("BullDogDog");

INSERT INTO Species (name)
VALUES
("Chat"),
("Chien"),
("Chienne");

INSERT INTO Role (name)
VALUES ("Président"), ("Bénévole"), ("Vétérinaire");

INSERT INTO User_role (Role_id, User_id, active, expiration, Center_id)
VALUES (1, 1, NOW(), NOW(), 1),
(2, 1, NOW(), NOW(), 1),
(3, 1, NOW(), NOW(), 1);

INSERT INTO Center_role (Role_id, Center_id)
VALUES (1, 1),
(2, 1),
(3, 1),
(2, 5),
(3, 5),
(1, 5);

INSERT INTO Animal (name, description, age, gender, birthDate, isActive, documentRef, Center_id, Race_id, Species_id) VALUES
("chien", "méchant", 10, "Male", NOW(), true, "ddde", 1, 1, 1),
("chien", "méchant", 10, "Male", NOW(), true, "ddde", 1, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 1, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 1, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 1, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 1, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 1, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 5, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 5, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 5, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 6, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 6, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 6, 1, 1);

INSERT INTO Picture (path, Animal_id, User_id, Offer_id, Center_id)
VALUES ("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", 1, null, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 1, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, null, null , 1),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 3, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 4, 1 , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 5, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 5, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 6, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 6, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 6, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 5, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", 1, null, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", 1, null, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", 1, null, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 2, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 3, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 4, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 5, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", 10, null, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", 10, null, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", 10, null, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 9, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 9, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 9, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, 9, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", 3, null, null , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, null, null , 3),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, null, 2 , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, null, 2 , null),
("https://as2.ftcdn.net/v2/jpg/02/15/84/43/1000_F_215844325_ttX9YiIIyeaR7Ne6EaLLjMAmy4GvPC69.jpg", null, null, 2 , null);

INSERT INTO Adoption (User_id, Animal_id, Animal_Center_id, isActive, adoptionDate) VALUES
(1, 1, 1, true, NOW()),
(1, 2, 1, false, NOW()),
(1, 3, 1, true, NOW()),
(1, 8, 5, true, NOW()),
(1, 9, 5, true, NOW()),
(1, 10, 5, true, NOW()),
(1, 4, 1, true, NOW()),
(2, 4, 1, true, NOW()),
(2, 4, 1, true, NOW());


INSERT INTO Vaccine (name) 
VALUES
("dataseet"),
("dataseed"),
("grou"),
("covidou"),
("gripou");

INSERT INTO Vaccine_center (Vaccine_id, Center_id)
VALUES
(1, 1),
(1, 2),
(4, 1),
(3, 1),
(5, 2),
(5, 5),
(1, 5);

INSERT INTO Center_species (Center_id, Species_id)
VALUES
(1, 1),
(1, 2),
(5, 1),
(5, 2);

INSERT INTO Center_race (Center_id, Race_id) 
VALUES
(1, 1),
(1, 2),
(5, 2),
(5, 1);

INSERT INTO Vaccine_animal (Vaccine_id, Animal_id, vaccinationDate, nextVaccination) 
VALUES (1, 1, NOW(), NOW() ),
(1, 2, NOW(), NOW() ),
(2, 1, NOW(), NOW() ),
(2, 2, NOW(), NOW() ),
(3, 2, NOW(), NOW() ),
(4, 2, NOW(), NOW() ),
(4, 3, NOW(), NOW() ),
(4, 12, NOW(), NOW() ),
(4, 11, NOW(), NOW() ),
(4, 13, NOW(), NOW() );


INSERT INTO Candidat (User_id, Offer_id, Offer_Center_id, landedDate, isAccepted, isActive)
VALUES (1, 1, 1, NOW(), true, false),
(2, 1, 1, NOW(), true, false),
(3, 1, 1, NOW(), true, false),
(1, 2, 1, NOW(), true, false),
(2, 2, 1, NOW(), true, false),
(3, 2, 1, NOW(), true, false),
(3, 3, 5, NOW(), true, false),
(2, 3, 5, NOW(), true, false),
(1, 3, 5, NOW(), true, false);



DROP TRIGGER IF EXISTS delete_addressFk;
-- Trigger déclenché par l'insertion
DELIMITER |
CREATE TRIGGER boegbjzol9xmzfhwchc7.delete_addressFk BEFORE DELETE
ON Address FOR EACH ROW
BEGIN
    UPDATE User SET Address_id = null WHERE User.Address_id = OLD.id;
END |

DROP TRIGGER IF EXISTS delete_center_address;
-- Trigger déclenché par l'insertion
DELIMITER |
CREATE TRIGGER boegbjzol9xmzfhwchc7.delete_center_address BEFORE DELETE
ON Address FOR EACH ROW
BEGIN
    UPDATE Center SET Address_id = null WHERE Center.Address_id = OLD.id;
END |


