Schéma de la base : ![Schéma de la base](./databaseSchema.JPG)
---
## Les Procédures stockées :

### Procédure stockées multiple :
**select_centers** (**limit INT, offset: INT**) retourne tous les centres en base de données, ainsi que leurs photos.

**select_animals** (**centerId: INT, limit: INT, offset: INT**) selectionne les animaux par centre, ainsi que leurs photos.

**select_countrys** (**limit: INT, offset: INT**) selectionne les pays disponible.

**select_user_roles** () selectionne les roles disponible.

**select_center_roles** (**center_id: INT**) selectione les roles lié à un centre?

**select_user_with_role_by_center** () selectionne les utilisateurs qui ont des rôles dans un centre.
            
**select_center_vaccine** (**center_id: INT**) selectionne les vaccins disponible qui sont lié à un centre.

**select_center_race** (**center_id: INT**) selectionne les races disponible qui sont lié à un centre.

**select_center_species** (**center_id: INT**) selectionne les espèces disponible qui sont lié à un centre.

**select_center_offer** (**center_id: INT**) selectionne les offres disponible dans un centre.

**select_offer_candidat** (**offer_id: INT**) selectionne les candidats disponible qui sont lié à une offre.

**select_candidat_offer** (**User_id: INT**) selectionne les offres d'un candidat.

**select_adoption_by_center** (**center_id: INT**) selectionne les adoptions d'un centre.

**select_adoption_by_user** (**user_id: INT**) selectionne les adoptions d'un utilisateur.

### Procédure stockées unique :
---
**select_animal** (**id: INT**) selectionne un animal.
**select_user** (**id: INT**) selectionne un utilisateur.

**select_address** (**id: INT**) selectionne une adresse.

**select_adoption** (**id: INT**) selectionne une adoption d'un centre.

**select_specie** (**id: INT**) selectionne une espèce disponible.

**select_race** (**id: INT**) selectionne une race disponible.

**select_vaccin** (**id: INT**) selectionne un vaccin disponible.

**select_offer** (**id: INT**) selectionne une offre.

**select_center** (**id: INT**) selectionne un centre.

### Procédure stockées de suppression :
---
**delete_user** (**id: INT**) supprime un utilisateur.

**delete_animal** (**id: INT**) supprime un animal.

**delete_offer** (**id: INT**) supprime une offre d'un centre.

**delete_address** (**id: INT**) supprime une adresse.

**delete_candidat** (**id: INT**) supprime un candidat.

**delete_picutre** (**id: INT**) supprime une photo.

**delete_role** (**id: INT**) supprime un rôle ainsi que sa connexion entre les utilisateurs et les centres.

**delete_vaccine** (**id: INT**) supprime un vaccin ainsi que sa connexion entre les centres, cependant, si un animal est associé au vaccin, la suppression ne va pas marcher.

**delete_race** (**id: INT**) supprime une race ainsi que sa connexion entre les centres, cependant, si un animal est associé à la race, la suppression ne va pas marcher.

**delete_species** (**id: INT**) supprime une espèce ainsi que sa connexion entre les centres, cependant, si un animal est associé à une espèce, la suppression ne va pas marcher.

#### Procédure stockées de modification (**UPDATE**)  :
---
**update_user** (**email: VARCHAR(70), lastName VARCHAR(60), phoneNumber VARCHAR(60), password VARCHAR(350), lastConnexion DATE, user_id INT**)   modifie un utilisateur.

**update_animal** (**name: VARCHAR(60), description VARCHAR(150), age VARCHAR(45), gender VARCHAR(30), birthDate DATE, isActive BOOLEAN, documentRef VARCHAR(75), Center_id INT, Race_id INT, Species_id INT, id INT**) modifie la fiche d'un animal.

**update_centre** (**email VARCHAR(70), name VARCHAR(60), phoneNumber VARCHAR(60), isActive BOOLEAN, schedule VARCHAR(150), Address_id INT, Country_id INT, id INT**) modifie la fiche d'un centre.

**update_picture** (**path VARCHAR(250), Animal_id INT, User_id INT, Offer_id INT, Center_id INT, id INT**) modifie une photo.

**update_adoption** (**User_id INT, Animal_id INT, Animal_Center_id INT, isActive BOOLEAN, adoptionDate DATE, id INT**) modifie une adoption

**update_candidat** (**User_id INT, Offer_id INT,  Offer_Center_id INT, landedDate DATE, isAccepted BOOLEAN, isActive BOOLEAN, id INT**) modifie un candidat.

**update_offer** (**name VARCHAR(70), description VARCHAR(400), startDate DATE, endDate DATE, place INT Center_id INT, id INT**) modifie une offre d'un centre.

**update_race** (**name VARCHAR(60), id INT**) modifie une race d'un centre.

**update_vaccine** (**name VARCHAR(60), id INT**) modifie le vaccin d'un centre.

**update_species** (**name VARCHAR(60), id INT**) 

**update_role** (**name VARCHAR(70), id INT**) 

**update_user_role** (**Role_id INT, User_id INT, active DATE, expiration DATE**) modifie les rôles d'un utilisateur

**update_address** (**postalCode, streetName, city, Country_id, id**)

