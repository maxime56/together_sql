
use boegbjzol9xmzfhwchc7;
-- //////////////////////////////
-- ******************************
-- PROCEDURE DE SELECTION UNIQUE
-- ******************************
-- //////////////////////////////

-- selectionne un utilisateur

DROP PROCEDURE IF EXISTS select_user;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user`(
	IN n_user_id INT
)
BEGIN  
	SELECT id, email, firstName, lastName, phoneNumber, lastConnexion, Address_id, password FROM User WHERE id = n_user_id;
END$$
DELIMITER ;


-- selectionne une adresse

DROP PROCEDURE IF EXISTS select_address;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_address`(
	IN n_address_id INT
)
BEGIN  
	SELECT * FROM Address WHERE id = n_address_id;
END$$
DELIMITER ;


-- selectionne une adoption

DROP PROCEDURE IF EXISTS select_adoption;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_adoption`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Adoption WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne une espèce

DROP PROCEDURE IF EXISTS select_specie;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_specie`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Species WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne une race

DROP PROCEDURE IF EXISTS select_race;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_race`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Race WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne un vaccin

DROP PROCEDURE IF EXISTS select_vaccin;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_vaccin`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Vaccine WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne une offre

DROP PROCEDURE IF EXISTS select_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_offer`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Offer WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne une offre

DROP PROCEDURE IF EXISTS select_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_center`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Center WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne une offre

DROP PROCEDURE IF EXISTS select_animal;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animal`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Animal WHERE id = n_id;
END$$
DELIMITER ;

-- selectionne un candidat

DROP PROCEDURE IF EXISTS select_candidat;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_candidat`(
	IN n_id INT,
	IN n_offferId INT
)
BEGIN  
	SELECT * FROM Candidat WHERE User_id = n_id AND Offer_id = n_offferId;
END$$
DELIMITER ;


-- selectionne une table de jointure

DROP PROCEDURE IF EXISTS select_animal_vaccined;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animal_vaccined`(
	IN n_VaccineId INT,
	IN n_Animal_id INT
)
BEGIN  
	SELECT * FROM Vaccine_animal WHERE Vaccine_id = n_VaccineId AND Animal_id = n_Animal_id;
END$$
DELIMITER ;


-- selectionne une photo

DROP PROCEDURE IF EXISTS select_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_picture`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Picture WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne un role 

DROP PROCEDURE IF EXISTS select_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_role`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Role WHERE id = n_id;
END$$
DELIMITER ;


-- selectionne un role 

DROP PROCEDURE IF EXISTS select_user_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user_role`(
	IN n_roleId INT,
	IN n_UserId INT
)
BEGIN  
	SELECT * FROM User_role WHERE Role_id = n_roleId AND User_id = n_UserId;
END$$
DELIMITER ;


-- permet de chercher les informations utiles d'un utilisateurs qui ont adoptés un animal

DROP PROCEDURE IF EXISTS select_user_information_adoption;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user_information_adoption`(
	IN n_UserId INT
)
BEGIN  
	SELECT DISTINCT User.id, User.firstName, User.lastName, User.email, User.phoneNumber FROM User INNER JOIN Adoption WHERE Adoption.User_id;
END$$
DELIMITER ;


-- récupère les pays

DROP PROCEDURE IF EXISTS get_countrys;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`get_countrys`(
)
BEGIN  
	SELECT * FROM Country ;
END$$
DELIMITER ;

-- récupère un centre

DROP PROCEDURE IF EXISTS get_one_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`get_one_center`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Center WHERE Center.id =  n_id;
END$$
DELIMITER ;

-- récupère les tous les pays qui ont un centre dans leur pays

DROP PROCEDURE IF EXISTS get_center_countrys;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`get_center_countrys`(
)
BEGIN  
	SELECT Country.name, Country.id FROM Center INNER JOIN Country WHERE Center.Country_id = Country.id;
END$$
DELIMITER ;

-- ///////////////////////////////////////////
-- ******************************************
-- FIN DES PROCEDURES DE SELECTION UNIQUE
-- ******************************************
-- ///////////////////////////////////////////
