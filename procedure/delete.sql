-- supprime un utilisateur

DROP PROCEDURE IF EXISTS delete_user;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_user`(
	IN n_id INT
)
BEGIN  
	DELETE FROM User WHERE id = n_id;
END$$
DELIMITER ;


-- supprime un animal

DROP PROCEDURE IF EXISTS delete_animal;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_animal`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Animal WHERE id = n_id;
END$$
DELIMITER ;


-- supprime une offre d'un centre

DROP PROCEDURE IF EXISTS delete_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_offer`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Offer WHERE id = n_id;
END$$
DELIMITER ;


-- supprime une adresse

DROP PROCEDURE IF EXISTS delete_address;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_address`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Address WHERE id = n_id;
END$$
DELIMITER ;


-- supprime un candidat

DROP PROCEDURE IF EXISTS delete_candidat;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_candidat`(
	IN n_id INT,
	IN n_offerId INT
)
BEGIN  
	DELETE FROM Candidat WHERE User_id = n_id AND Offer_id = n_offerId;
END$$
DELIMITER ;


-- supprime les candidatures d'un utilisateur

DROP PROCEDURE IF EXISTS delete_all_user_application;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_all_user_application`(
	IN n_userId INT
)
BEGIN  	
	DELETE FROM Candidat WHERE User_id = n_userId;
END$$
DELIMITER ;


-- supprime une photo

DROP PROCEDURE IF EXISTS delete_picutre;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_picutre`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Picture WHERE id = n_id;
END$$
DELIMITER ;

-- supprime les photos d'un utisateur (il doit avoir normalement une seul photo)

DROP PROCEDURE IF EXISTS delete__user_picutre;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete__user_picutre`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Picture WHERE User_id = n_id;
END$$
DELIMITER ;


-- supprime un rôle ainsi que sa connexion entre les utilisateurs et les centres

DROP PROCEDURE IF EXISTS delete_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_role`(
	IN n_role_id INT
)
BEGIN  
	DELETE FROM Role WHERE id = n_role_id;
END$$
DELIMITER ;


-- supprime un vaccin ainsi que sa connexion entre les centres, cependant, si un animal est associé au vaccin, la suppression ne va pas marcher

DROP PROCEDURE IF EXISTS delete_vaccine;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_vaccine`(
	IN n_vaccine_id INT
)
BEGIN  
	DELETE FROM Vaccine WHERE id = n_vaccine_id;
END$$
DELIMITER ;


-- supprime une race ainsi que sa connexion entre les centres, cependant, si un animal est associé à la race, la suppression ne va pas marcher

DROP PROCEDURE IF EXISTS delete_race;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_race`(
	IN n_race_id INT
)
BEGIN  
 	DELETE FROM Race WHERE id = n_race_id;
END$$
DELIMITER ;


-- supprime une espèce ainsi que sa connexion entre les centres, cependant, si un animal est associé à une espèce, la suppression ne va pas marcher

DROP PROCEDURE IF EXISTS delete_species;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_species`(
	IN n_species_id INT
)
BEGIN  
 	DELETE FROM Species WHERE id = n_species_id;
END$$
DELIMITER ;


-- supprime un animal vacciné

DROP PROCEDURE IF EXISTS delete_animal_vaccined;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_animal_vaccined`(
	IN n_Vaccine_id INT,
	IN n_Animal_id INT
)
BEGIN  
 	DELETE FROM Vaccine_animal WHERE Vaccine_id = n_Vaccine_id AND Animal_id = n_Animal_id;
END$$
DELIMITER ;


-- supprime un userRole

DROP PROCEDURE IF EXISTS delete_userRole;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_userRole`(
	IN n_Role_id INT,
	IN n_User_id INT
)
BEGIN 
 	DELETE FROM User_role WHERE Role_id = n_Role_id AND User_id = n_User_id;
END$$
DELIMITER ;use boegbjzol9xmzfhwchc7;


-- supprime toutes les offres d'un centre
DROP PROCEDURE IF EXISTS delete_center_offers;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_offers`(
	IN n_id INT
)
BEGIN  
	DELETE Picture FROM boegbjzol9xmzfhwchc7.Picture INNER JOIN Offer WHERE Offer.id = Picture.Offer_id AND Offer.Center_id = n_id;
	DELETE FROM Offer WHERE Center_id = n_id;
END$$
DELIMITER ;


-- supprime les photos d'un animal

DROP PROCEDURE IF EXISTS delete_animal_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_animal_picture`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Picture WHERE Animal_id = n_id;
END$$
DELIMITER ;



-- supprime les races qui sont liés à un centre
DROP PROCEDURE IF EXISTS delete_center_races;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_races`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Center_race WHERE Center_id = n_id;
END$$
DELIMITER ;

-- supprime la table de jointure Center_species 

DROP PROCEDURE IF EXISTS delete_one_center_vaccine;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_one_center_vaccine`(
	IN n_Center_id INT,
	IN n_Vaccine_id INT
)
BEGIN  
	DELETE FROM Vaccine_center WHERE Center_id = n_Center_id AND Vaccine_id = n_Vaccine_id;
END$$
DELIMITER ;

-- supprime la table de jointure Center_species 

DROP PROCEDURE IF EXISTS delete_one_center_species;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_one_center_species`(
	IN n_Center_id INT,
	IN n_Species_id INT
)
BEGIN  
	DELETE FROM Center_species WHERE Center_id = n_Center_id AND Species_id = n_Species_id;
END$$
DELIMITER ;

-- supprime les photos d'une offre

DROP PROCEDURE IF EXISTS delete_offer_pictures;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_offer_pictures`(
	IN n_Offer_id INT
)
BEGIN  
	DELETE FROM Picture WHERE Offer_id = n_Offer_id;
END$$
DELIMITER ;

-- supprime la table de jointure Center_race 

DROP PROCEDURE IF EXISTS delete_one_center_race;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_one_center_race`(
	IN n_Center_id INT,
	IN n_Race_id INT
)
BEGIN  
	DELETE FROM Center_race WHERE Center_id = n_Center_id AND Race_id = n_Race_id;
END$$
DELIMITER ;

-- supprime le ou les adoptions d'un animal
DROP PROCEDURE IF EXISTS delete_animal_adoption;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_animal_adoption`(
	IN n_AnimalId INT
)
BEGIN  
	DELETE FROM Adoption WHERE Animal_id = n_AnimalId;
END$$
DELIMITER ;
-- supprime les photos d'un centre 

DROP PROCEDURE IF EXISTS delete_center_pictures;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_pictures`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Picture WHERE Center_id = n_id;
END$$
DELIMITER ;

-- supprime les vaccins qui sont liés à un centre
DROP PROCEDURE IF EXISTS delete_center_vaccines;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_vaccines`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Vaccine_center WHERE Center_id = n_id;
END$$
DELIMITER ;

-- supprime les roles qui sont liés à un centre
DROP PROCEDURE IF EXISTS delete_center_userRole;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_userRole`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Center_role WHERE Center_id = n_id;
    DELETE FROM User_role WHERE Center_id = n_id;
END$$
DELIMITER ;

-- supprime les especes qui sont liés à un centre


DROP PROCEDURE IF EXISTS delete_center_animals;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_animals`(
	IN n_id INT
)
BEGIN  
	DELETE Vaccine_animal FROM boegbjzol9xmzfhwchc7.Vaccine_animal INNER JOIN Animal WHERE Animal.id = Vaccine_animal.Animal_id AND Animal.Center_id = n_id;
	DELETE Picture FROM boegbjzol9xmzfhwchc7.Picture INNER JOIN Animal WHERE Animal.id = Picture.Animal_id AND Animal.Center_id = n_id;
	DELETE FROM Animal Where Center_id = n_id;
END$$
DELIMITER ;

-- supprime les especes qui sont liés à un centre

DROP PROCEDURE IF EXISTS delete_center_species;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_species`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Center_species WHERE Center_id = n_id;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS delete_center_adoptions;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_adoptions`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Adoption WHERE Animal_Center_id = n_id;
END$$
DELIMITER ;


-- supprime un centre
DROP PROCEDURE IF EXISTS delete_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Center WHERE id = n_id;
END$$
DELIMITER ;


-- supprime les candidats qui ont postulés aux offres d'un centre

DROP PROCEDURE IF EXISTS delete_center_candidat;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`delete_center_candidat`(
	IN n_id INT
)
BEGIN  
	DELETE FROM Candidat WHERE Offer_Center_id = n_id;
END$$
DELIMITER ;