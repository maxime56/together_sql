use boegbjzol9xmzfhwchc7;


-- modifie un utilisateur

DROP PROCEDURE IF EXISTS update_user;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_user`(
    IN n_email VARCHAR(70),
    IN n_firstName VARCHAR(60),
    IN n_lastName VARCHAR(60),
    IN n_phoneNumber VARCHAR(60),
    IN n_password VARCHAR(350),
    IN n_address_id INT,
	IN n_user_id INT
)
BEGIN  
    UPDATE User
	SET 
    email = n_email,
    firstName = n_firstName,
    lastName = n_lastName,
    phoneNumber = n_phoneNumber,
    password = n_password,
    Address_id = n_address_id
	WHERE id = n_user_id;
END$$
DELIMITER ;

-- permet d'update l'id d'une adresse à un utilisateur

DROP PROCEDURE IF EXISTS link_user_addressId;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`link_user_addressId`(
    IN n_Address_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE User
	SET 
    Address_id = n_Address_id
	WHERE id = n_id;
END$$
DELIMITER ;


-- modifie une adresse

DROP PROCEDURE IF EXISTS update_address;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_address`(
    IN n_postalCode VARCHAR(50),
    IN n_streetName VARCHAR(60),
    IN n_city VARCHAR(60),
    IN n_Country_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE Address
	SET 
    postalCode = n_postalCode,
    streetName = n_streetName,
    city = n_city,
    Country_id = n_Country_id
	WHERE id = n_id;
END$$
DELIMITER ;


-- modifie la fiche d'un animal

DROP PROCEDURE IF EXISTS update_animal;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_animal`(
    IN n_name VARCHAR(60),
    IN n_description VARCHAR(150),
    IN n_age VARCHAR(45),
    IN n_gender VARCHAR(30),
    IN n_birthDate DATE,
    IN n_isActive BOOLEAN,
    IN n_documentRef VARCHAR(75),
    IN n_Center_id INT,
    IN n_Race_id INT,
    IN n_Species_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE Animal
	SET 
    name = n_name,
    description = n_description,
    age = n_age,
    gender = n_gender,
    birthDate = n_birthDate,
    isActive = n_isActive,
    documentRef = n_documentRef,
    Center_id = n_Center_id,
    Race_id = n_Race_id,
    Species_id = n_Species_id
    WHERE id = n_id;
END$$
DELIMITER ;


-- modifie la fiche d'un centre

DROP PROCEDURE IF EXISTS update_centre;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_centre`(
    IN n_email VARCHAR(70),
    IN n_name VARCHAR(60),
    IN n_phoneNumber VARCHAR(60),
    IN n_isActive BOOLEAN,
    IN n_schedule VARCHAR(150),
    IN n_Address_id INT,
    IN n_Country_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE Center
	SET 
    email = n_email,
    name = n_name,
    phoneNumber = n_phoneNumber,
    isActive = n_isActive,
    schedule = n_schedule,
    Address_id = n_Address_id,
    Country_id = n_Country_id
    WHERE id = n_id;
END$$
DELIMITER ;




-- modifie une photo

DROP PROCEDURE IF EXISTS update_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_picture`(
    IN n_path VARCHAR(250),
    IN n_Animal_id INT,
    IN n_User_id INT,
    IN n_Offer_id INT,
    IN n_Center_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE Picture
	SET 
    path = n_path,
    Animal_id = n_Animal_id,
    User_id = n_User_id,
    Offer_id = n_Offer_id,
    Center_id = n_Center_id
    WHERE id = n_id;
END$$
DELIMITER ;


-- modifie une adoption

DROP PROCEDURE IF EXISTS update_adoption;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_adoption`(
    IN n_User_id INT,
    IN n_Animal_id INT,
    IN n_Animal_Center_id INT,
    IN n_isActive BOOLEAN,
    IN n_adoptionDate DATE,
	IN n_id INT
)
BEGIN   
    UPDATE Adoption
	SET 
    User_id = n_User_id,
    Animal_id = n_Animal_id,
    Animal_Center_id = n_Animal_Center_id,
    isActive = n_isActive,
    adoptionDate = n_adoptionDate
    WHERE id = n_id;
END$$
DELIMITER ;


-- modifie un candidat

DROP PROCEDURE IF EXISTS update_candidat;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_candidat`(
    IN n_User_id INT,
    IN n_Offer_id INT,
    IN n_Offer_Center_id INT,
    IN n_landedDate DATE,
    IN n_isAccepted BOOLEAN,
    IN n_isActive BOOLEAN
)
BEGIN  
    UPDATE Candidat
	SET 
    User_id = n_User_id,
    Offer_id = n_Offer_id,
    Offer_Center_id = n_Offer_Center_id,
    landedDate = n_landedDate,
    isAccepted = n_isAccepted,
    isActive = n_isActive
    WHERE User_id = n_User_id AND Offer_id = n_Offer_id;
END$$
DELIMITER ;



-- modifie un candidat

DROP PROCEDURE IF EXISTS update_candidat;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_candidat`(
    IN n_User_id INT,
    IN n_Offer_id INT,
    IN n_Offer_Center_id INT,
    IN n_landedDate DATE,
    IN n_isAccepted BOOLEAN,
    IN n_isActive BOOLEAN
)
BEGIN  
    UPDATE Candidat
	SET 
    User_id = n_User_id,
    Offer_id = n_Offer_id,
    Offer_Center_id = n_Offer_Center_id,
    landedDate = n_landedDate,
    isAccepted = n_isAccepted,
    isActive = n_isActive
    WHERE User_id = n_User_id AND Offer_id = n_Offer_id;
END$$
DELIMITER ;


-- modifie une offre

DROP PROCEDURE IF EXISTS update_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_offer`(
    IN n_name VARCHAR(70),
    IN n_description VARCHAR(400),
    IN n_startDate DATE,
    IN n_endDate DATE,
    IN n_place INT,
    IN n_Center_id INT,
    IN n_id INT
)
BEGIN  
    UPDATE Offer
	SET 
    name = n_name,
    description = n_description,
    startDate = n_startDate,
    endDate = n_endDate,
    place = n_place,
    Center_id = n_Center_id
    WHERE id = n_id;
END$$
DELIMITER ;


-- modifie une race d'un centre

DROP PROCEDURE IF EXISTS update_race;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_race`(
    IN n_name VARCHAR(60),
    IN n_id INT
)
BEGIN  
    UPDATE Race
	SET 
    name = n_name
    WHERE id = n_id;
END$$
DELIMITER ;

-- modifie une espèce d'un centre

DROP PROCEDURE IF EXISTS update_species;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_species`(
    IN n_name VARCHAR(60),
    IN n_id INT
)
BEGIN  
    UPDATE Species
	SET 
    name = n_name
    WHERE id = n_id;
END$$
DELIMITER ;


-- modifie un vaccin d'un centre

DROP PROCEDURE IF EXISTS update_vaccine;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_vaccine`(
    IN n_name VARCHAR(60),
    IN n_id INT
)
BEGIN  
    UPDATE Vaccine
	SET 
    name = n_name
    WHERE id = n_id;
END$$
DELIMITER ;

-- modifie la fiche d'un animal vacciné d'un centre

DROP PROCEDURE IF EXISTS update_vaccined_animal;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_vaccined_animal`(
    IN n_Vaccine_id INT,
    IN n_Animal_id INT,
    IN n_vaccinationDate DATE,
    IN n_nextVaccination DATE
)
BEGIN  
    UPDATE Vaccine_animal
	SET 
    vaccinationDate = n_vaccinationDate,
    nextVaccination = n_nextVaccination
    WHERE Vaccine_id = n_Vaccine_id AND Animal_id = n_Animal_id;
END$$
DELIMITER ;

-- modifie un role d'un centre

DROP PROCEDURE IF EXISTS update_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_role`(
    IN n_name VARCHAR(70),
    IN n_id INT
)
BEGIN  
    UPDATE Role
	SET 
    name = n_name
    WHERE id = n_id;
END$$
DELIMITER ;

-- modifie un role d'un centre

DROP PROCEDURE IF EXISTS update_user_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`update_user_role`(
   IN n_Role_id INT,
   IN n_User_id INT,
   IN n_active DATE,
   IN n_expiration DATE,
   IN n_Center_id INT
)
BEGIN  
    UPDATE User_role
	SET 
    active = n_active,
    expiration = n_expiration,
    Center_id = n_Center_id
    WHERE Role_id = n_Role_id AND User_id = n_User_id;
END$$
DELIMITER ;

-- permet d'update l'id d'une adresse à un center

DROP PROCEDURE IF EXISTS link_center_addressId;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`link_center_addressId`(
  IN n_Address_id INT,
	IN n_id INT
)
BEGIN  
    UPDATE Center
	SET 
    Address_id = n_Address_id
	WHERE id = n_id;
END$$
DELIMITER ;