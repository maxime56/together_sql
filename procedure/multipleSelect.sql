use boegbjzol9xmzfhwchc7;

-- //////////////////////////////
-- ******************************
-- PROCEDURE DE SELECTION MULTIPLE
-- ******************************
-- //////////////////////////////


-- selectionne les centres

DROP PROCEDURE IF EXISTS select_centers;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_centers`(
    IN n_limit INT,
	IN n_offset INT
)
BEGIN
    SELECT * FROM Center LIMIT n_limit OFFSET n_offset;
END$$
DELIMITER ;


-- selectionne les centres avec les adresses

DROP PROCEDURE IF EXISTS select_centers_with_address_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_centers_with_address_picture`(
	IN n_addressId INT
)
BEGIN
    SELECT * FROM Center INNER JOIN Address WHERE Center.Address_id = Address.id;
END$$
DELIMITER ;


-- selectionne les animaux par centre

DROP PROCEDURE IF EXISTS select_animals;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animals`(
	IN id_center INT,
    IN n_limit INT,
	IN n_offset INT
)
BEGIN
	SELECT * FROM Animal WHERE Center_id = id_center LIMIT n_limit OFFSET n_offset;
END$$
DELIMITER ;


-- selectionne les pays disponible

DROP PROCEDURE IF EXISTS select_countrys;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_countrys`(
    IN n_limit INT,
	IN n_offset INT
)
BEGIN
    SELECT * FROM Country LIMIT n_limit OFFSET n_offset;
END$$
DELIMITER ;

-- selectionne les roles disponible

DROP PROCEDURE IF EXISTS select_roles_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_roles_by_center`(
	IN n_center_id INT
)
BEGIN
    SELECT * FROM Center_role INNER JOIN Role WHERE Role.id = Center_role.Role_id AND  Center_id = n_center_id;
END$$
DELIMITER ;



-- selectionne les roles disponible qui sont lié à un centre

DROP PROCEDURE IF EXISTS select_center_roles;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_center_roles`(
	IN n_center_id INT
)
BEGIN
    SELECT * FROM Role INNER JOIN Center_role ON Center_role.Center_id = n_center_id;
END$$
DELIMITER ;


-- selectionne la table de jointure les utilisateurs qui ont des rôles dans un centre

DROP PROCEDURE IF EXISTS select_user_with_role_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user_with_role_by_center`(
	IN n_center_id INT
)
BEGIN
	SELECT DISTINCT * FROM User_role WHere Center_id = n_center_id;
END$$
DELIMITER ;


-- selectionne les vaccins disponible qui sont lié à un centre

DROP PROCEDURE IF EXISTS select_center_vaccine;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_center_vaccine`(
	IN centerId INT
)
BEGIN
	SELECT DISTINCT * FROM Vaccine INNER JOIN Vaccine_center WHERE Vaccine_center.Center_id = centerId AND Vaccine.id = Vaccine_center.Vaccine_id;
END$$
DELIMITER ;

-- selectionne les races disponible qui sont lié à un centre

DROP PROCEDURE IF EXISTS select_all_races_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_races_by_center`(
	IN CenterId INT
)
BEGIN
    SELECT DISTINCT * FROM Race INNER JOIN Center_race WHERE Center_race.Center_id = CenterId AND Center_race.Race_id = Race.id;
END$$
DELIMITER ;

-- selectionne les races disponible

DROP PROCEDURE IF EXISTS select_all_races;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_races`(

)
BEGIN
    SELECT * FROM Race;
END$$
DELIMITER ;

-- selectionne les espèces disponible

DROP PROCEDURE IF EXISTS select_all_species;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_species`(

)
BEGIN
    SELECT * FROM Species;
END$$
DELIMITER ;

-- selectionne les Vaccins disponible

DROP PROCEDURE IF EXISTS select_all_vaccines;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_vaccines`(

)
BEGIN
    SELECT * FROM Vaccine;
END$$
DELIMITER ;

-- selectionne les Roles disponible

DROP PROCEDURE IF EXISTS select_all_roles;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_roles`(

)
BEGIN
    SELECT * FROM Role;
END$$
DELIMITER ;


-- selectionne les espèces disponible qui sont lié à un centre

DROP PROCEDURE IF EXISTS select_all_species_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_all_species_by_center`(
	IN n_center_id INT
)
BEGIN
    SELECT DISTINCT * FROM Species inner join Center_species WHERE Center_species.Center_id = n_center_id AND Center_species.Species_id = Species.id;
END$$
DELIMITER ;


-- selectionne les offres dans un centre

DROP PROCEDURE IF EXISTS select_center_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_center_offer`(
	IN n_center_id INT
)
BEGIN
    SELECT * FROM Offer WHERE Center_id = n_center_id;
END$$
DELIMITER ;

-- selectionne les candidats ont postulés à une offre

DROP PROCEDURE IF EXISTS select_candidats_by_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_candidats_by_offer`(
	IN n_offer_id INT
)
BEGIN
    SELECT * FROM Candidat INNER JOIN User WHERE Candidat.User_id = User.id AND Offer_id = n_offer_id;
END$$
DELIMITER ;

-- selectionne les offres d'un candidat

DROP PROCEDURE IF EXISTS select_candidat_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_candidat_offer`(
	IN n_User_id INT
)
BEGIN  
	SELECT * FROM Offer INNER JOIN Candidat ON Candidat.User_id = n_User_id AND Candidat.Offer_id = id;
END$$
DELIMITER ;

-- selectionne les adoptions d'un centre

DROP PROCEDURE IF EXISTS select_adoption_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_adoption_by_center`(
	IN n_center_id INT
)
BEGIN  
	SELECT * FROM Adoption WHERE Animal_Center_id = n_center_id;
END$$
DELIMITER ;


-- selectionne les adoptions d'un utilisateur

DROP PROCEDURE IF EXISTS select_adoption_by_user;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_adoption_by_user`(
	IN n_user_id INT
)
BEGIN  
	SELECT * FROM Adoption WHERE User_id = n_user_id;
END$$
DELIMITER ;


-- selectionne les animaux vaccinés par un même vaccin

DROP PROCEDURE IF EXISTS select_animals_vaccined_by_vaccine;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animals_vaccined_by_vaccine`(
	IN n_Center_id INT,
	IN n_Vaccine_id INT
)
BEGIN  
	SELECT * FROM Vaccine_animal INNER JOIN Animal WHERE Animal.id = Vaccine_animal.Animal_id AND Vaccine_id = n_Vaccine_id AND Animal.Center_id = n_Center_id;
END$$
DELIMITER ;


-- selectionne les animaux vaccinés par centre

DROP PROCEDURE IF EXISTS select_animals_vaccined_by_center;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animals_vaccined_by_center`(
	IN n_center_id INT
)
BEGIN  
	SELECT DISTINCT * FROM Vaccine_animal INNER JOIN Animal WHERE Vaccine_animal.Animal_id = Animal.id AND Animal.Center_id = n_center_id;
END$$
DELIMITER ;


-- selectionne les photos d'un centre

DROP PROCEDURE IF EXISTS select_center_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_center_picture`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Picture WHERE Center_id = n_id;
END$$
DELIMITER ;


-- selectionne les photos d'un utilisateur

DROP PROCEDURE IF EXISTS select_user_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user_picture`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Picture WHERE User_id = n_id;
END$$
DELIMITER ;

-- selectionne les photos d'un animal

DROP PROCEDURE IF EXISTS select_animal_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_animal_picture`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Picture WHERE Animal_id = n_id;
END$$
DELIMITER ;


-- selectionne les photos d'une offre

DROP PROCEDURE IF EXISTS select_offer_picture;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_offer_picture`(
	IN n_id INT
)
BEGIN  
	SELECT * FROM Picture WHERE Offer_id = n_id;
END$$
DELIMITER ;


-- selectionne un utilisateur qui possèdent un rôle dans un centre
DROP PROCEDURE IF EXISTS select_user_with_role;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_user_with_role`(
	IN n_userId INT
)
BEGIN  
	SELECT * FROM User_role WHERE User_id = n_userId;
END$$
DELIMITER ;


-- selectionne les offres auquel un utilisateur à participer

DROP PROCEDURE IF EXISTS select_historic_offer;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_historic_offer`(
	IN n_userId INT
)
BEGIN  
	SELECT * FROM Candidat INNER JOIN Offer WHERE User_id = n_userId;
END$$
DELIMITER ;


-- selectionne les centres par pays

DROP PROCEDURE IF EXISTS select_country_centers;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_country_centers`(
	IN n_limit INT,
	IN n_offset INT,
	IN n_countryId INT
)
BEGIN  
	SELECT * FROM Center WHERE Country_id = n_countryId LIMIT n_limit OFFSET n_offset;
END$$
DELIMITER ;

-- selectionne les centres

DROP PROCEDURE IF EXISTS select_every_centers;
DELIMITER $$
CREATE PROCEDURE `boegbjzol9xmzfhwchc7`.`select_every_centers`(
	IN n_limit INT,
	IN n_offset INT
)
BEGIN  
	SELECT * FROM Center LIMIT n_limit OFFSET n_offset;
END$$
DELIMITER ;


-- ///////////////////////////////////////////
-- ******************************************
-- FIN DES PROCEDURES DE SELECTION MULTIPLE
-- ******************************************
-- ///////////////////////////////////////////
