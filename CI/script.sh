#!/bin/bash
echo "DATABASE"
echo "USE $DB;"| mysql -h $HOST -P $PORT -u $USER --password=$PASSWORD < ./database.sql
echo "PROCEDURE DELETE"
echo "USE $DB;"| mysql -h $HOST -P $PORT -u $USER --password=$PASSWORD < ./procedure/delete.sql
echo "PROCEDURE MULTIPLE SELECT"
echo "USE $DB;"| mysql -h $HOST -P $PORT -u $USER --password=$PASSWORD < ./procedure/multipleSelect.sql
echo "PROCEDURE SELECT"
echo "USE $DB;"| mysql -h $HOST -P $PORT -u $USER --password=$PASSWORD < ./procedure/select.sql
echo "PROCEDURE UPDATE"
echo "USE $DB;"| mysql -h $HOST -P $PORT -u $USER --password=$PASSWORD < ./procedure/update.sql
echo "DATASET COUNTRY"
echo "USE $DB;"| mysql -h $HOST -P $PORT -u $USER --password=$PASSWORD < ./dataset/insertCountry.sql
echo "DATASET"
echo "USE $DB;"| mysql -h $HOST -P $PORT -u $USER --password=$PASSWORD < ./dataset/insertData.sql
