-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema boegbjzol9xmzfhwchc7
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema spa
-- -----------------------------------------------------
CREATE DATABASE IF NOT EXISTS `boegbjzol9xmzfhwchc7`;

USE `boegbjzol9xmzfhwchc7`;

-- -----------------------------------------------------
-- Table `mydb`.`Address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Address`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Address` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `postalCode` VARCHAR(50) NOT NULL,
  `streetName` VARCHAR(60) NOT NULL,
  `city` VARCHAR(60) NOT NULL,
  `country` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`User`;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(70) NOT NULL,
  `firstName` VARCHAR(60) NOT NULL,
  `lastName` VARCHAR(60) NOT NULL,
  `phoneNumber` VARCHAR(60) NOT NULL,
  `password` VARCHAR(350) NOT NULL,
  `lastConnexion` DATE NOT NULL,
  `Address_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_User_Address_idx` (`Address_id` ASC),
  CONSTRAINT `fk_User_Address`
    FOREIGN KEY (`Address_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Center`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center`;

CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(70) NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  `phoneNumber` VARCHAR(60) NOT NULL,
  `isActive` TINYINT NOT NULL,
  `schedule` VARCHAR(150) NOT NULL,
  `Address_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Center_Address1_idx` (`Address_id` ASC),
  CONSTRAINT `fk_Center_Address1`
    FOREIGN KEY (`Address_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Race`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Race`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Race` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Species`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Species`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Species` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Animal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Animal`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Animal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `description` VARCHAR(150) NOT NULL,
  `age` VARCHAR(45) NOT NULL,
  `gender` VARCHAR(30) NOT NULL,
  `birthDate` DATE NOT NULL,
  `isActive` TINYINT NOT NULL,
  `documentRef` VARCHAR(75) NOT NULL,
  `Center_id` INT NOT NULL,
  `Race_id` INT NOT NULL,
  `Species_id` INT NOT NULL,
  PRIMARY KEY (`id`, `Center_id`, `Race_id`, `Species_id`),
  INDEX `fk_Animal_Center1_idx` (`Center_id` ASC),
  INDEX `fk_Animal_Race1_idx` (`Race_id` ASC),
  INDEX `fk_Animal_Species1_idx` (`Species_id` ASC),
  CONSTRAINT `fk_Animal_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Animal_Race1`
    FOREIGN KEY (`Race_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Race` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Animal_Species1`
    FOREIGN KEY (`Species_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Species` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Center_Species`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center_Species`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center_Species` (
  `Center_id` INT NOT NULL,
  `Species_id` INT NOT NULL,
  PRIMARY KEY (`Center_id`, `Species_id`),
  INDEX `fk_Center_has_Species_Species1_idx` (`Species_id` ASC),
  INDEX `fk_Center_has_Species_Center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_Center_has_Species_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Center_has_Species_Species1`
    FOREIGN KEY (`Species_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Species` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Center_Race`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center_Race`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center_Race` (
  `Center_id` INT NOT NULL,
  `Race_id` INT NOT NULL,
  PRIMARY KEY (`Center_id`, `Race_id`),
  INDEX `fk_Center_has_Race_Race1_idx` (`Race_id` ASC),
  INDEX `fk_Center_has_Race_Center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_Center_has_Race_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Center_has_Race_Race1`
    FOREIGN KEY (`Race_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Race` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Offer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Offer`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Offer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL,
  `description` VARCHAR(400) NOT NULL,
  `startDate` DATE NOT NULL,
  `endDate` DATE NOT NULL,
  `place` INT NOT NULL,
  `Center_id` INT NOT NULL,
  PRIMARY KEY (`id`, `Center_id`),
  INDEX `fk_Offer_Center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_Offer_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Candidat`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Candidat`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Candidat` (
  `User_id` INT NOT NULL,
  `Offer_id` INT NOT NULL,
  `Offer_Center_id` INT NOT NULL,
  `landedDate` DATE NOT NULL,
  `isAccepted` TINYINT NOT NULL,
  `isActive` TINYINT NOT NULL,
  PRIMARY KEY (`User_id`, `Offer_id`, `Offer_Center_id`),
  INDEX `fk_User_has_Offer_Offer1_idx` (`Offer_id` ASC, `Offer_Center_id` ASC),
  INDEX `fk_User_has_Offer_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_User_has_Offer_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Offer_Offer1`
    FOREIGN KEY (`Offer_id` , `Offer_Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Offer` (`id` , `Center_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Picture`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Picture`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Picture` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(120) NOT NULL,
  `User_id` INT NOT NULL,
  PRIMARY KEY (`id`, `User_id`),
  INDEX `fk_Picture_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_Picture_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Animal_Pictures`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Anial_Pictures`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Animal_Pictures` (
  `Animal_id` INT NOT NULL,
  `Picture_id` INT NOT NULL,
  PRIMARY KEY (`Animal_id`, `Picture_id`),
  INDEX `fk_Animal_has_Picture_Picture1_idx` (`Picture_id` ASC),
  INDEX `fk_Animal_has_Picture_Animal1_idx` (`Animal_id` ASC),
  CONSTRAINT `fk_Animal_has_Picture_Animal1`
    FOREIGN KEY (`Animal_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Animal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Animal_has_Picture_Picture1`
    FOREIGN KEY (`Picture_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Picture` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Offer_Picture`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Offer_Picture`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Offer_Picture` (
  `Offer_id` INT NOT NULL,
  `Picture_id` INT NOT NULL,
  PRIMARY KEY (`Offer_id`, `Picture_id`),
  INDEX `fk_Offer_has_Picture_Picture1_idx` (`Picture_id` ASC),
  INDEX `fk_Offer_has_Picture_Offer1_idx` (`Offer_id` ASC),
  CONSTRAINT `fk_Offer_has_Picture_Offer1`
    FOREIGN KEY (`Offer_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Offer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Offer_has_Picture_Picture1`
    FOREIGN KEY (`Picture_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Picture` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Center_Picture`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Center_Picture`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Center_Picture` (
  `Center_id` INT NOT NULL,
  `Picture_id` INT NOT NULL,
  PRIMARY KEY (`Center_id`, `Picture_id`),
  INDEX `fk_Center_has_Picture_Picture1_idx` (`Picture_id` ASC),
  INDEX `fk_Center_has_Picture_Center1_idx` (`Center_id` ASC),
  CONSTRAINT `fk_Center_has_Picture_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Center_has_Picture_Picture1`
    FOREIGN KEY (`Picture_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Picture` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Vaccine`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Vaccine_Animal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_Animal`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_Animal` (
  `Vaccine_id` INT NOT NULL,
  `Animal_id` INT NOT NULL,
  `vaccinationDate` DATE NOT NULL,
  `nextVaccination` DATE NOT NULL,
  PRIMARY KEY (`Vaccine_id`, `Animal_id`),
  INDEX `fk_Vaccine_has_Animal_Animal1_idx` (`Animal_id` ASC),
  INDEX `fk_Vaccine_has_Animal_Vaccine1_idx` (`Vaccine_id` ASC),
  CONSTRAINT `fk_Vaccine_has_Animal_Vaccine1`
    FOREIGN KEY (`Vaccine_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Vaccine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Vaccine_has_Animal_Animal1`
    FOREIGN KEY (`Animal_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Animal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`Vaccine_Center`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_Center`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`Vaccine_Center` (
  `Vaccine_id` INT NOT NULL,
  `Center_id` INT NOT NULL,
  PRIMARY KEY (`Vaccine_id`, `Center_id`),
  INDEX `fk_Vaccine_has_Center_Center1_idx` (`Center_id` ASC),
  INDEX `fk_Vaccine_has_Center_Vaccine1_idx` (`Vaccine_id` ASC),
  CONSTRAINT `fk_Vaccine_has_Center_Vaccine1`
    FOREIGN KEY (`Vaccine_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Vaccine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Vaccine_has_Center_Center1`
    FOREIGN KEY (`Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `boegbjzol9xmzfhwchc7`.`adoption`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `boegbjzol9xmzfhwchc7`.`adoption`;
CREATE TABLE IF NOT EXISTS `boegbjzol9xmzfhwchc7`.`adoption` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `User_id` INT NOT NULL,
  `Animal_id` INT NOT NULL,
  `Animal_Center_id` INT NOT NULL,
  `isActive` TINYINT NOT NULL,
  `adoptionDate` DATE NOT NULL,
  PRIMARY KEY (`id`, `User_id`, `Animal_id`, `Animal_Center_id`),
  INDEX `fk_User_has_Animal_Animal1_idx` (`Animal_id` ASC, `Animal_Center_id` ASC),
  INDEX `fk_User_has_Animal_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_User_has_Animal_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Animal_Animal1`
    FOREIGN KEY (`Animal_id` , `Animal_Center_id`)
    REFERENCES `boegbjzol9xmzfhwchc7`.`Animal` (`id` , `Center_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
