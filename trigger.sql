USE boegbjzol9xmzfhwchc7;
DROP TRIGGER IF EXISTS delete_addressFk;
-- Trigger déclenché par l'insertion
DELIMITER |
CREATE TRIGGER boegbjzol9xmzfhwchc7.delete_addressFk BEFORE DELETE
ON Address FOR EACH ROW
BEGIN
    UPDATE User SET Address_id = null WHERE User.Address_id = OLD.id;
END |

DROP TRIGGER IF EXISTS delete_center_address;
-- Trigger déclenché par l'insertion
DELIMITER |
CREATE TRIGGER boegbjzol9xmzfhwchc7.delete_center_address BEFORE DELETE
ON Address FOR EACH ROW
BEGIN
    UPDATE Center SET Address_id = null WHERE Center.Address_id = OLD.id;
END |